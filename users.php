<?php
  $page_title = 'Lista de usuarios';
  require_once('includes/load.php');
?>
<?php
// Checkin What level user has permission to view this page
    $modulo=2;
require_once('permiso.php');
 page_require_level(1);
//pull out all user form database
 $all_users = find_all_user();
?>
<?php include_once('layouts/header.php'); ?>
  <?php if($row_permiso['RolVer']==0) {echo "No tiene permiso ";} else { ?>
<div class="row">
   <div class="col-md-12">
     <?php echo display_msg($msg); ?>
   </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading clearfix">
        <strong>
          <span class="glyphicon glyphicon-th"></span>
          <span>Usuarios</span>
       </strong>
         <a href="add_user.php" class="btn btn-info pull-right" <?php if($row_permiso['RolAgregar']==0){echo 'style="pointer-events: none"';} ?>>Agregar usuario</a>
      </div>
      <div class="col-md-12 right">
      <label class="control-label" for="">Buscar:</label>
      <input type="text" class="form-control search" name="search" id="search">
      </div>
     <div class="panel-body">
      <table class="table table-bordered table-striped">
        <thead>
          <tr>
            <th class="text-center" style="width: 50px;">#</th>
            <th>Nombre </th>
            <th>Usuario</th>
            <th class="text-center" style="width: 15%;">Rol de usuario</th>
            <th class="text-center" style="width: 15%;">Fecha de creaación</th>
            <th class="text-center" style="width: 15%;">Creado por</th>
            <th class="text-center" style="width: 10%;">Estado</th>
            <th style="width: 20%;">Último login</th>
            <th class="text-center" style="width: 100px;">Acciones</th>
          </tr>
        </thead>
        <tbody id="data_user">
        <?php foreach($all_users as $a_user): ?>
          <tr>
           <td class="text-center"><?php echo count_id();?></td>
           <td><?php echo remove_junk(ucwords($a_user['name']))?></td>
           <td><?php echo remove_junk(ucwords($a_user['username']))?></td>
           <td class="text-center"><?php echo remove_junk(ucwords($a_user['group_name']))?></td>
           <td class="text-center"><?php echo remove_junk(ucwords($a_user['date']))?></td>
           <td class="text-center"><span class="label label-created_by"> <?php echo remove_junk(ucwords($a_user['create_by']))?></span></td>
           <td class="text-center">
           <?php if($a_user['status'] === '1'): ?>
            <span class="label label-success"><?php echo "Activo"; ?></span>
          <?php else: ?>
            <span class="label label-danger"><?php echo "Inactivo"; ?></span>
          <?php endif;?>
           </td>
           <td><?php echo read_date($a_user['last_login'])?></td>
           <td class="text-center">
             <div class="btn-group">
                <a href="edit_user.php?id=<?php echo (int)$a_user['id'];?>" class="btn btn-xs btn-warning" data-toggle="tooltip" title="Editar" <?php if($row_permiso['RolEditar']==0){echo 'style="pointer-events: none"';} ?>>
                  <i class="glyphicon glyphicon-pencil"></i>
               </a>
                <!-- <a  iduser=<?php echo (int)$a_user['id'];?> class="btn btn-xs btn-danger" data-toggle="tooltip" title="Eliminar">
                    <i class="glyphicon glyphicon-remove"></i>
                </a> -->
                </div>
           </td>
          </tr>
        <?php endforeach;?>
       </tbody>
     </table>
     </div>
    </div>
  </div>
</div>
  <?php } include_once('layouts/footer.php'); ?>
    <script type="text/javascript">
  $('.btn-danger').click(function(event){
    var $id = $( this );
    alertify.confirm("Desea eliminar usuario?.",
        function(){
          
          let idUser = $id.attr( "iduser" );
          if(idUser) {
            alertify.success('Ok');
            window.location.href="delete_user.php?id=" + $id.attr( "iduser" )
          } else {
            alertify.error('No hay id especificado');
          }
        },
         function(){
          alertify.error('Cancel');
        });
  })
    </script>
