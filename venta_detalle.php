<?php
$page_title = 'Agregar venta';
require_once('includes/load.php');
require_once('includes/conex.php');


$numeroventa=0;
$total = 0;

$query_venta = "SELECT * FROM sales WHERE  input_part = '{$_GET['venta']}'";
$venta = mysqli_query($conex, $query_venta) or die(mysqli_error($conex));
$row_venta= mysqli_fetch_assoc($venta);

$query_movimientos = find_by_id('movsalida',(int)$row_venta['MovSalidaId']); 

$query_cliente = "SELECT ClienteId, ClienteNombre, ClienteDireccion FROM clientes WHERE ClienteId='{$row_venta['ClienteId']}'";
$cliente = mysqli_query($conex, $query_cliente) or die(mysqli_error($conex));
$row_cliente = mysqli_fetch_assoc($cliente);

$query_productos = "SELECT id, product_id, qty, price, ClienteId, VentaTotal FROM sales WHERE input_part = '{$_GET['venta']}'";
$productos = mysqli_query($conex, $query_productos) or die(mysqli_error($conex));
$row_productos= mysqli_fetch_assoc($productos); 

$query_descripcion = "SELECT VentaDescripcion FROM sales ORDER BY id DESC LIMIT 1";
$descripcion = mysqli_query($conex, $query_descripcion) or die(mysqli_error($conex));
$row_descripcion= mysqli_fetch_assoc($descripcion); 

$query_user = "SELECT name FROM users WHERE id ='{$row_venta['VentaUsuario']}'";
$user = mysqli_query($conex, $query_user) or die(mysqli_error($conex));
$row_user = mysqli_fetch_assoc($user);	


?>
<?php include_once('layouts/header.php'); ?>

<div class="row">
  <div class="col-md-6">
    <div class="form-group"><br>
         <div class="input-group">
         <table border="0" cellpadding="0" cellspacing="0">
             <tr>
               <td><span style="font-weight: bold">PARTE DE ENTRADA</span></td>
               <td><?php printf('%08d', $row_venta['input_part']); ?></td>
             </tr>
             <tr>
              <td><span style="font-weight: bold">TIPO MOVIMIENTO:</span> </td>
               <td><?php echo $query_movimientos['name']; ?></td>
             </tr>
             <tr>
               <td width="157"><span style="font-weight: bold">NOMBRE:</span></td>
               <td width="199"><?php echo $row_cliente['ClienteNombre']; ?></td>
             </tr>
             <tr>
               <td><span style="font-weight: bold">DIRECCIÓN:</span></td>
               <td><?php echo $row_cliente['ClienteDireccion']; ?></td>
             </tr>
             <tr>
               <td><span style="font-weight: bold">FECHA:</span></td>
               <td><?php echo $row_venta['date']; ?></td>
             </tr>
             <tr>
              <td><span style="font-weight: bold">DOC REFERENCIA:</span> </td>
               <td><?php echo $row_venta['VentaDoc']; ?></td>
             </tr>
              <tr>
              <td><span style="font-weight: bold">USUARIO:</span> </td>
               <td><?php echo $row_user['name']; ?></td>
             </tr>
             <tr>
              <td><span style="font-weight: bold">DESCRIPCIÓN:</span> </td>
               <td><?php echo $row_venta['VentaDescripcion']; ?></td>
             </tr>
             <tr>
               <td colspan="2"><br></td>
               </tr>
           </table>                 
        </div> 
      </div> 
  </div> 
  <div class="col-md-6"> <a href="registro_ventas.php" class=" pull-right btn btn-primary">Regresar</a>
      <a href="reporte.php?venta=<?php echo $row_venta['input_part'] ?>" target="_blank" class=" pull-right btn btn-danger">Imprimir</a>
       
  </div>
</div>
  

<div class="row">

  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading clearfix">
        <strong>
          <span class="glyphicon glyphicon-th"></span>
          <span>Productos</span>
        </strong>
      </div>
      <div class="panel-body">

        <form method="post" action="add_sale.php">
       <table class="table table-bordered">
            <thead>
              <th colspan="3"> Producto </th>
              <!-- <th> <div align="right">Precio </div></th> -->
              <th> <div align="center">Cantidad </div></th>
              <!-- <th> <div align="right">Total </div></th> -->
              </thead>
            <tbody id="product_info"> </tbody>
            <?php 		
			do { 
						$query_productolista = "SELECT name, sale_price FROM products WHERE id = '{$row_productos['product_id']}'";
$productolista = mysqli_query($conex, $query_productolista) or die(mysqli_error($conex));
$row_productolista= mysqli_fetch_assoc($productolista); 
			?>
                 <tr>
              <td colspan="3"> <?php echo $row_productolista['name']; ?> </td>
              <!-- <td> <div align="right"><?php echo $row_productolista['sale_price']; ?> </div></td> -->
              <td> <div align="center"><?php echo $row_productos['qty']; ?> </div></td>
              <!-- <td> <div align="right">
                <?php
			  // $total = $row_productolista['sale_price'] * $row_productos['qty'] + $total; 
			  $total =$row_productos['qty'] + $total; 
			   echo number_format($row_productolista['sale_price'] * $row_productos['qty'], 2, '.',',');?> 
              </div></td> -->
              </tr>           
           <?php } while ($row_productos = mysqli_fetch_assoc($productos)); ?>
            <tr>
              <td colspan="3"> <div align="right"><span style="font-weight: bold">TOTAL</span></div></td>
              <td><div align="center"><span style="font-weight: bold"><?php echo number_format($total, 0, '.',',');; ?></span> </div></td>
              </tr>     
          </table>
        </form>
      </div>
    </div>
  </div>

</div>

<?php include_once('layouts/footer.php'); ?>

<?php
mysqli_free_result($venta);
mysqli_free_result($cliente);
mysqli_free_result($descripcion);
?>
