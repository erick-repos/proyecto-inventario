<?php
$page_title = 'Kardex por Movimientos';
require_once('includes/load.php');
require_once('includes/conex.php');
// Checkin What level user has permission to view this page

$hoy = date("Y-m-d");
$primerdia = date("Y-m-01");
$n = 1;
$cantidadE = 0;
$cantidadS = 0;
$cantidadSal = 0;
$preciototalE = 0;
$preciototalS = 0;
$preciototalSal = 0;

if (!isset($_GET['desde'])) {
  $_GET['desde'] = '0000-00-00';
}
if (!isset($_GET['hasta'])) {
  $_GET['hasta'] = '0000-00-00';
}
if (!isset($_GET['prod'])) {
  $_GET['prod'] = '-1';
}

$query_producto = "SELECT id,pmedida, name FROM products WHERE code='{$_GET['prod']}'";
$producto = mysqli_query($conex, $query_producto) or die(mysqli_error($conex));
$row_producto = mysqli_fetch_assoc($producto);

$query_unidades = "SELECT * FROM unidadmedida WHERE id ='{$row_producto['pmedida']}'";
$unidades = mysqli_query($conex, $query_unidades) or die(mysqli_error($conex));
$row_unidades = mysqli_fetch_assoc($unidades);

$query_movimientos = "SELECT CompraDescripcion, date, price, Tipo, qty, CompraPU, ProductoStock, CompraTotal, ProveedorId FROM shop WHERE product_id = '{$row_producto['id']}' AND DATE(date)>='{$_GET['desde']}' AND DATE(date)<='{$_GET['hasta']}' UNION ALL SELECT VentaDescripcion, date, price, Tipo, qty, VentaPU, ProductoStock, Ventatotal, ClienteId FROM sales WHERE product_id = '{$row_producto['id']}' AND DATE(date)>='{$_GET['desde']}' AND DATE(date)<='{$_GET['hasta']}' order BY date ASC";
$movimientos = mysqli_query($conex, $query_movimientos) or die(mysqli_error($conex));
$row_movimientos = mysqli_fetch_assoc($movimientos);

$totalRows_movimientos = mysqli_num_rows($movimientos);

$cantidadSal = $cantidadSal + $row_movimientos['ProductoStock'];

  $modulo=17;
require_once('permiso.php');
include_once('layouts/header.php') ?>
  <?php if($row_permiso['RolVer']==0) {echo "No tiene permiso ";} else { ?>
<div class="row">
  <div class="col-md-6">
    <?php echo display_msg($msg); ?>
  </div>
</div>
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading clearfix">
          <strong>
            <span class="glyphicon glyphicon-th"></span>
            <span>Kardex</span>
          </strong>
        </div>
        <div class="panel-body kardex">
        <form action="kardex.php" method="get" name="form1" id="form1">
  <div class="row">
    <div class="col-md-6">


      <table border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td>
            <div class="input-group">
              <span class="input-group-btn">
                <span class="btn btn-primary">Desde</span>
              </span>
              <input name="desde" type="date" class="form-control" id="desde" value="<?php if (($_GET['desde'] <> "0000-00-00")) {
                                                                                        echo $_GET['desde'];
                                                                                      } else {
                                                                                        echo $primerdia;
                                                                                      } ?>" required>
            </div>
          </td>
          <td>
            <div class="input-group">
              <span class="input-group-btn">
                <span type="submit" class="btn btn-primary">Hasta</span>
              </span>
              <input name="hasta" type="date" class="form-control" id="hasta" value="<?php if (($_GET['hasta'] <> "0000-00-00")) {
                                                                                        echo $_GET['hasta'];
                                                                                      } else {
                                                                                        echo $hoy;
                                                                                      } ?>" required>
            </div>
          </td>
        </tr>
      </table>


    </div>
    <div class="col-md-6">

      <div class="input-group">
        <span class="input-group-btn">
          <span type="submit" class="btn btn-primary">Código</span>
        </span>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td><input name="prod" type="text" class="form-control" id="prod" value="<?php if ($_GET['prod'] <> '-1') {
                                                                                        echo $_GET['prod'];
                                                                                      } ?>" required></td>
            <td>
              <div align="center">
                <input name="Enviar" type="submit" value="Buscar" class="btn btn-primary" />
              </div>
            </td>
          </tr>
        </table>
      </div>
    </div>
  </div>
</form>

<?php if ($totalRows_movimientos > 0) { ?>
 
          <div class="pull-center">
            <div align="right"><a href="reporte.php?desde=<?php echo $_GET['desde']; ?>&hasta=<?php echo $_GET['hasta']; ?>&prod=<?php echo $_GET['prod']; ?>" target="_blank" class="btn btn-danger">Imprimir</a> </div>
          </div>
        </div>
        <div class="panel-body kardex">
          <strong>Producto: </strong><?php echo $row_producto['name']; ?>
          <br />
          <br />
          <table class="table table-bordered">
            <tr>
              <td>
                <div align="center"><strong>Item</strong></div>
              </td>
              <td><strong>Fecha</strong></td>
              <td><strong>Detalle</strong></td>
              <td colspan="5">
                <div align="center"><strong>Entradas</strong></div>
              </td>
              <td colspan="5">
                <div align="center"><strong>Salidas</strong></div>
              </td>
              <td colspan="3">
                <div align="center"><strong>Saldo</strong></div>
              </td>
            </tr>
            <tr>
              <td colspan="3">
                <div align="center"></div>
              </td>
              <td colspan="3">
                <div align="center"><strong>Proveedor</strong></div>
              </td>
              <td >
              <div align="center"><strong>Ud. Medida</strong></div></td>
              <td><div align="center"><strong>CANT.</strong></div></td>
              <!-- <td>
                <div align="center"><strong>PU</strong></div>
              </td> -->
              <!-- <td>
                <div align="center"><strong>PT</strong></div>
              </td> -->
              <td colspan="3">
                <div align="center"><strong>Cliente</strong></div>
              </td>
              <td >
              <div align="center"><strong>Ud. Medida</strong></div></td>
              <td><div align="center"><strong>CANT.</strong></div></td>
              <!-- <td>
                <div align="center"><strong>PU</strong></div>
              </td>
              <td>
                <div align="center"><strong>PT</strong></div>
              </td> -->
              <td>
                <div align="center"><strong>CANT.</strong></div>
              </td>
              <!-- <td>
                <div align="center"><strong>PU</strong></div>
              </td>
              <td>
                <div align="center"><strong>PT</strong></div>
              </td> -->
            </tr>
            <?php do { ?>
              <tr>
                <td>
                  <div align="center"><?php echo $n ?></div>
                </td>
                <td><?php echo $row_movimientos['date'] ?></td>
                <td><?php echo $row_movimientos['CompraDescripcion'] ?></td>
                <td colspan="3">
                  <div align="center"><?php

                                      $query_proveedor = "SELECT name FROM proveedores WHERE id='{$row_movimientos['ProveedorId']}'";
                                      $proveedor = mysqli_query($conex, $query_proveedor) or die(mysqli_error($conex));
                                      $row_proveedor = mysqli_fetch_assoc($proveedor);

                                      if ($row_movimientos['Tipo'] == "C") {
                                        echo $row_proveedor['name'];
                                      } ?></div>
                </td>
                <td><?php if ($row_movimientos['Tipo'] == "C") {
                                        echo $row_unidades['name'];
                                      } ?></td>
                <td><?php if ($row_movimientos['Tipo'] == "C") {
                      echo $row_movimientos['qty'];
                      $cantidadE = $cantidadE + $row_movimientos['qty'];
                    } ?></td>
                <!-- <td>
                  <div align="center">
                    <?php if ($row_movimientos['Tipo'] == "C") {
                      echo number_format($row_movimientos['CompraPU'], 2, '.', ',');
                    } ?>
                  </div>
                </td>
                <td>
                  <div align="center">
                    <?php if ($row_movimientos['Tipo'] == "C") {
                      echo number_format($row_movimientos['CompraTotal'], 2, '.', ',');
                      $preciototalE = $preciototalE + $row_movimientos['CompraTotal'];
                    } ?>
                  </div>
                </td> -->
                <td colspan="3">
                  <div align="center"><?php
                                      $query_cliente = "SELECT ClienteNombre FROM clientes WHERE ClienteId='{$row_movimientos['ProveedorId']}'";
                                      $cliente = mysqli_query($conex, $query_cliente) or die(mysqli_error($conex));
                                      $row_cliente = mysqli_fetch_assoc($cliente);

                                      if ($row_movimientos['Tipo'] == "V") {
                                        echo $row_cliente['ClienteNombre'];
                                      } ?></div>
                </td>
                < <td><?php if ($row_movimientos['Tipo'] == "V") {
                                        echo $row_unidades['name'];
                                      } ?></td>
                <td align="center"><?php if ($row_movimientos['Tipo'] == "V") {
                      echo $row_movimientos['qty'];
                      $cantidadS = $cantidadS + $row_movimientos['qty'];
                    } ?></td>
                <!-- <td>
                  <div align="center">
                    <?php if ($row_movimientos['Tipo'] == "V") {
                      echo number_format($row_movimientos['CompraPU'], 2, '.', ',');
                    } ?>
                  </div>
                </td>
                <td>
                  <div align="center">
                    <?php if ($row_movimientos['Tipo'] == "V") {
                      echo number_format($row_movimientos['CompraTotal'], 2, '.', ',');
                      $preciototalS = $preciototalS + $row_movimientos['CompraTotal'];
                    } ?>
                  </div>
                </td> -->
                <td colspan="3">
                  <div align="center"><?php echo $row_movimientos['ProductoStock'];
                                      $cantidadSal =  $row_movimientos['ProductoStock']; ?></div>
                                                                          
                </td>
                <!-- <td>
                  <div align="center"><?php echo number_format($row_movimientos['CompraPU'], 2, '.', ','); ?></div>
                </td>
                <td>
                  <div align="center"><?php echo number_format($row_movimientos['ProductoStock'] * $row_movimientos['CompraPU'], 2, '.', ',');
                                      $preciototalSal = ($row_movimientos['ProductoStock'] * $row_movimientos['CompraPU']); ?></div>
                </td> -->
              </tr>
            <?php
              $n++;
            } while ($row_movimientos = mysqli_fetch_assoc($movimientos)); ?>
            <tr>
              <td>
                <div align="center"></div>
              </td>
              <td colspan="6">
                <div align="right"><strong>TOTALES</strong></div>
              </td>
              <td colspan="1">
                <div align="center"><strong><?php echo $cantidadE; ?></strong></div>
              </td>
              <td colspan="4">&nbsp;</td>
              <!-- <td>
                <div align="center"></div> 
              </td> 
              <td>
                <div align="center"><strong><?php echo number_format($preciototalE, 2, '.', ','); ?></strong></div>
              </td>  -->
            
              <td colspan="1">
                <div align="center"><strong><?php echo $cantidadS; ?></strong></div>
              </td>
              <!-- <td>
                <div align="center"></div>
              </td> -->
              <!-- <td>
                <div align="center"><strong><?php echo number_format($preciototalS, 2, '.', ','); ?></strong></div>
              </td> -->
              <td>
                <div align="center"><strong><?php echo $cantidadSal; ?></strong></div>
              </td>
              <!-- <td>
                <div align="center"></div>
              </td> -->
              <!-- <td>
                <div align="center"><strong><?php echo number_format($preciototalSal, 2, '.', ','); ?></strong></div>
              </td> -->
            </tr>
          </table>
        
      
    
  
<?php } ?>

        </div>
      </div>
    </div>
  </div>

<?php } include_once('layouts/footer.php'); ?>


