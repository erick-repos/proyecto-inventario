<?php
$page_title = 'Agregar venta';
require_once('includes/load.php');
// Checkin What level user has permission to view this page
page_require_level(3);
?>
<?php

if (isset($_POST['add_sale'])) {
  if (empty($errors)) {
    $rs = $db->query("SELECT `input_part` FROM `sales` order by id desc LIMIT 1");
    $row = mysqli_fetch_row($rs);
    $count = 0;
    foreach ($_POST['s_id'] as $index => $id) {
      $p_id = $db->escape($id);
      $cid = $row[0] + 1;
      $s_qty = $db->escape($_POST['quantity'][$index]);
      $s_total = $db->escape($_POST['total'][$index]);
      $date = $db->escape($_POST['date'][$index]);
      $s_date = make_date();
      $sql  = "INSERT INTO sales (";
      $sql .= " product_id,input_part,qty,price,date ";
      $sql .= ") VALUES (";
      $sql .= "'{$p_id}','{$cid}','{$s_qty}','{$s_total}','{$s_date}'";
      $sql .= ")";
      if ($db->query($sql)) {
        update_product_qty($s_qty, $p_id);
        $count++;
      }
    }
     if ($count > 0) {
      $session->msg('s', "Venta agregada ");
      redirect('sales.php', false);
    } else {
      $session->msg('d', 'Lo siento, registro falló.');
      redirect('add_sale.php', false);
    }
  } else {
    $session->msg("d", $errors);
    redirect('add_sale.php', false);
  }
}
?>
<?php include_once('layouts/header.php'); ?>
<div class="row">
  <div class="col-md-6">
    <?php echo display_msg($msg); ?>
    <form method="post" action="ajax.php" autocomplete="off" id="sug-form">
      <div class="form-group">
        <div class="input-group">
          <span class="input-group-btn">
            <button type="submit" class="btn btn-primary">Búsqueda</button>
          </span>
          <input type="text" id="sug_input" class="form-control" name="title" placeholder="Buscar por el nombre del producto">

        </div>
        <div id="result" class="list-group"></div>
      </div>
    </form>
  </div>
  <div class="col-md-6">
    <a href="sales.php" class="pull-right btn btn-primary">Show all sales</a>
  </div>
</div>
<div class="row">

  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading clearfix">
        <strong>
          <span class="glyphicon glyphicon-th"></span>
          <span>Editar venta</span>
        </strong>
      </div>
      <div class="panel-body">

        <form method="post" action="add_sale.php">
          <div class="btn-right">
            <button type="submit" 
            name="add_sale" 
            class="btn btn-primary">Agregar</button>
          </div>
          <table class="table table-bordered">
            <thead>
              <th> Producto </th>
              <th> Precio </th>
              <th> Cantidad </th>
              <th> Total </th>
              <th> Agregado</th>
            </thead>
            <tbody id="product_info"> </tbody>
          </table>
        </form>
      </div>
    </div>
  </div>

</div>

<?php include_once('layouts/footer.php'); ?>