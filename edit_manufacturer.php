<?php
  $page_title = 'Editar categoría';
  require_once('includes/load.php');
  // Checkin What level user has permission to view this page
  page_require_level(1);
?>
<?php
  //Display all catgories.
  $manufacturer = find_by_id('manufacturer',(int)$_GET['id']);
  $all_manufacturers = find_all('manufacturer');
  if(!$manufacturer){
    $session->msg("d","Missing manufacturer id.");
    redirect('manufacturer.php');
  }
?>

<?php
if(isset($_POST['edit_manu'])){
  $req_field = array('manufacturer-status');
  validate_fields($req_field);
  $manu_status = remove_junk($db->escape($_POST['manufacturer-status']));
  if(empty($errors)){
        $sql = "UPDATE manufacturer SET status='{$manu_status}' ";
       $sql .= " WHERE id='{$manufacturer['id']}'";
     $result = $db->query($sql);
     if($result && $db->affected_rows() === 1) {
       $session->msg("s", "Fabricante actualizado con éxito.");
       redirect('manufacturer.php',false);
     } else {
       $session->msg("d", "Lo siento, actualización falló.");
       redirect('manufacturer.php',false);
     }
  } else {
    $session->msg("d", $errors);
    redirect('manufacturer.php',false);
  }
}
?>
<?php include_once('layouts/header.php'); ?>

<div class="row">
   <div class="col-md-12">
     <?php echo display_msg($msg); ?>
   </div>
   <div class="col-md-5">
     <div class="panel panel-default">
       <div class="panel-heading">
         <strong>
           <span class="glyphicon glyphicon-th"></span>
           <span>Editando <?php echo remove_junk(ucfirst($manufacturer['name']));?></span>
        </strong>
       </div>
       <div class="panel-body">
         <form method="post" action="edit_manufacturer.php?id=<?php echo (int)$manufacturer['id'];?>">
           <div class="form-group">
           <input type="text" class="form-control" readonly name="manufacturer-name" value="<?php echo remove_junk(ucfirst($manufacturer['name']));?>">
          
           </div>
           <div class="form-group">
              <label for="status">Estado</label>
                <select class="form-control" name="manufacturer-status">
                  <option <?php if($manufacturer['status'] === '1') echo 'selected="selected"';?>value="1">Activo</option>
                  <option <?php if($manufacturer['status'] === '0') echo 'selected="selected"';?> value="0">Inactivo</option>
                </select>
            </div>
           <button type="submit" name="edit_manu" class="btn btn-primary">Actualizar Fabricante</button>
       </form>
       </div>
     </div>
   </div>
</div>



<?php include_once('layouts/footer.php'); ?>
