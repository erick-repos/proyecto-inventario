<?php
  $page_title = 'Editar Proveedor';
  require_once('includes/load.php');
  // Checkin What level user has permission to view this page
?>
<?php
  $cliente = find_by_cliente_id('Clientes',(int)$_GET['id']);
  if(!$cliente){
    $session->msg("d","Missing Cliente id.");
    redirect('edit_cliente.php');
  }
?>
<?php
  if(isset($_POST['update'])){
   $req_fields = array('status');
   validate_fields($req_fields);
   if(empty($errors)){
           $name = remove_junk($db->escape($_POST['name']));
           $dni = remove_junk($db->escape($_POST['dni']));
           $address = remove_junk($db->escape($_POST['address']));
         $status = remove_junk($db->escape($_POST['status']));
        $query  = "UPDATE clientes SET ";
        $query .= "ClienteNombre='{$name}',ClienteDNI='{$dni}',ClienteDireccion='{$address}',ClienteStatus='{$status}'";
        $query .= "WHERE ClienteId='{$db->escape($cliente['ClienteId'])}'";
        $result = $db->query($query);
         if($result && $db->affected_rows() === 1){
          //sucess
          $session->msg('s',"Cliente se ha actualizado Exitosamente! ");
          redirect('clientes.php?id='.(int)$supplier['id'], false);
        } else {
          //failed
          $session->msg('d','Lamentablemente no se ha actualizado el Cliente!');
          redirect('edit_cliente.php?id='.(int)$supplier['id'], false);
        }
   } else {
     $session->msg("d", $errors);
    redirect('edit_cliente.php?id='.(int)$supplier['id'], false);
   }
 }
?>
<?php include_once('layouts/header.php'); ?>
<div class="login-page">
    <div class="text-center">
       <h3>Editar Proveedor</h3>
     </div>
     <?php echo display_msg($msg); ?>
      <form method="post" action="edit_cliente.php?id=<?php echo (int)$cliente['ClienteId'];?>" class="clearfix">
        <div class="form-group">
              <label for="name" class="control-label">Nombre</label>
              <input type="name" class="form-control" name="name"  value="<?php echo remove_junk(ucwords($cliente['ClienteNombre'])); ?>">
        </div>
        <div class="form-group">
              <label for="level" class="control-label">DNI</label>
              <input type="text" class="form-control" name="dni"  value="<?php echo (int)$cliente['ClienteDNI']; ?>">
        </div>
        <div class="form-group">
              <label for="level" class="control-label">Dirección</label>
              <input type="text" class="form-control" name="address"  value="<?php echo remove_junk(ucwords($cliente['ClienteDireccion'])); ?>">
        </div>
        <div class="form-group">
          <label for="status">Estado</label>
              <select class="form-control" name="status">
                <option <?php if($cliente['ClienteStatus'] === '1') echo 'selected="selected"';?> value="1"> Activo </option>
                <option <?php if($cliente['ClienteStatus'] === '0') echo 'selected="selected"';?> value="0">Inactivo</option>
               <!--  <option <?php if($cliente['ClienteStatus'] === '0') echo 'selected="selected"';?> value="0">Inactivo</option> -->
              </select>
        </div>
        <div class="form-group clearfix">
                <button type="submit" name="update" class="btn btn-info">Actualizar</button>
        </div>
    </form>
</div>

<?php include_once('layouts/footer.php'); ?>
