<?php
$page_title = 'Detalle Compra';
require_once('includes/load.php');
require_once('includes/conex.php');


$numeroventa=0;
$total = 0;

$row_compra= find_by_part_number('shop',$_GET['compra']);
$query_movimientos = find_by_id('moventrada',(int)$row_compra['MovEntradaId']); 
$query_proveedor = "SELECT id, name, address FROM proveedores WHERE id='{$row_compra['ProveedorId']}'";
$proveedor = mysqli_query($conex, $query_proveedor) or die(mysqli_error($conex));
$row_proveedor = mysqli_fetch_assoc($proveedor);

$query_productos = "SELECT id, product_id, qty, price, id, CompraTotal FROM shop WHERE input_part = '{$_GET['compra']}'";
$productos = mysqli_query($conex, $query_productos) or die(mysqli_error($conex));
$row_productos= mysqli_fetch_assoc($productos); 

// $query_user = "SELECT name FROM users WHERE id ='{$row_compra['CompraUsuario']}'";
// $user = mysqli_query($conex, $query_user) or die(mysqli_error($conex));
$row_user = find_by_id('users',(int)$row_compra['CompraUsuario']);	


?>
<?php include_once('layouts/header.php'); ?>

<div class="row">
  <div class="col-md-6">
    <div class="form-group"><br>
         <div class="input-group">
           <table border="0" cellpadding="0" cellspacing="0">
             <tr>
               <td><span style="font-weight: bold">PARTE DE ENTRADA</span></td>
               <td><?php printf('%08d', $row_compra['input_part']); ?></td>
             </tr>
             <tr>
              <td><span style="font-weight: bold">TIPO MOVIMIENTO:</span> </td>
               <td><?php echo $query_movimientos['name']; ?></td>
             </tr>
             <tr>
               <td width="157"><span style="font-weight: bold">NOMBRE:</span></td>
               <td width="199"><?php echo $row_proveedor['name']; ?></td>
             </tr>
             <tr>
               <td><span style="font-weight: bold">DIRECCIÓN:</span></td>
               <td><?php echo $row_proveedor['address']; ?></td>
             </tr>
             <tr>
               <td><span style="font-weight: bold">FECHA:</span></td>
               <td><?php echo $row_compra['date']; ?></td>
             </tr>
             <tr>
              <td><span style="font-weight: bold">DOC REFERENCIA:</span> </td>
               <td><?php echo $row_compra['CompraDoc']; ?></td>
             </tr>
              <tr>
              <td><span style="font-weight: bold">USUARIO:</span> </td>
               <td><?php echo $row_user['name']; ?></td>
             </tr>
             <tr>
              <td><span style="font-weight: bold">DESCRIPCIÓN:</span> </td>
               <td><?php echo $row_compra['CompraDescripcion']; ?></td>
             </tr>
             <tr>
               <td colspan="2"><br></td>
               </tr>
           </table>             
        </div> 
      </div> 
  </div> 
  <div class="col-md-6"> <a href="registro_compras.php" class=" pull-right btn btn-primary">Regresar</a>
      <a href="reporte.php?compra=<?php echo $row_compra['input_part'] ?>" target="_blank" class=" pull-right btn btn-danger">Imprimir</a>
       
  </div>
</div>
  

<div class="row">

  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading clearfix">
        <strong>
          <span class="glyphicon glyphicon-th"></span>
          <span>Productos</span>
        </strong>
      </div>
      <div class="panel-body">

        <form method="post" action="add_sale.php">
       <table class="table table-bordered">
            <thead>
              <th colspan="3"> Producto </th>
              <!-- <th> <div align="right">Precio </div></th> -->
              <th> <div align="center">Cantidad </div></th>
              <!-- <th> <div align="right">Total </div></th> -->
              </thead>
            <tbody id="product_info"> </tbody>
            <?php 		
			do { 
						$query_productolista = "SELECT name, sale_price FROM products WHERE id = '{$row_productos['product_id']}'";
$productolista = mysqli_query($conex, $query_productolista) or die(mysqli_error($conex));
$row_productolista= mysqli_fetch_assoc($productolista); 
			?>
                 <tr>
              <td colspan="3"> <?php echo $row_productolista['name']; ?> </td>
              <!-- <td> <div align="right"><?php echo $row_productolista['sale_price']; ?> </div></td> -->
              <td> <div align="center"><?php echo $row_productos['qty']; ?> </div></td>
              <!-- <td> <div align="right">
                <?php
			  $total =  $row_productos['qty'] + $total; 
			   echo number_format($row_productolista['sale_price'] * $row_productos['qty'], 2, '.',',');
         ?> 
              </div></td> -->
              </tr>   
           <?php } while ($row_productos = mysqli_fetch_assoc($productos)); ?>
            <tr>
              <td colspan="3"> <div align="right"><span style="font-weight: bold">TOTAL</span></div></td>
              <td><div align="center"><span style="font-weight: bold"><?php echo number_format($total, 2, '.',',');; ?> </span> </div></td>
              </tr>     
          </table>
        </form>
      </div>
    </div>
  </div>

</div>

<?php include_once('layouts/footer.php'); ?>

<?php
mysqli_free_result($proveedor);
?>
