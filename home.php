<?php 
  $page_title = 'Home Page';
  require_once('includes/load.php');
  if (!$session->isUserLoggedIn(true)) { redirect('index.php', false);}
?>
<?php include_once('layouts/header.php'); ?>
<div class="row">
  <div class="col-md-12">
    <?php echo display_msg($msg); ?>
  </div>
 <div class="col-md-12">
    <div class="panel">
      <div class="jumbotron text-center">
        <?php $user = current_user(); ?>
          <?php if($user['user_level'] === '1'): ?>
        <!-- admin menu -->
          <h1>Dashboard Administrador</h1>
          <?php elseif($user['user_level'] === '2'): ?>
            <!-- Special user -->
            <h1>Dashboard Almacén</h1>
          <?php elseif($user['user_level'] === '3'): ?>
            <!-- User menu -->
            <h1>Dashboard Ventas</h1>
          <?php endif;?>
      </div>
    </div>
 </div>
</div>
<?php include_once('layouts/footer.php'); ?>
