<?php
$page_title = 'Salidas mensuales';
require_once('includes/load.php');
// Checkin What level user has permission to view this page
$modulo=15;
require_once('permiso.php');
page_require_level(3);
?>
<?php
$year = date('Y');
$sales = monthlySales($year);
?>
<?php include_once('layouts/header.php'); ?>
  <?php if($row_permiso['RolVer']==0) {echo "No tiene permiso ";} else { ?>
<div class="row">
  <div class="col-md-6">
    <?php echo display_msg($msg); ?>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading clearfix">
        <strong>
          <span class="glyphicon glyphicon-th"></span>
          <span>Salidas mensuales por producto</span>
        </strong>
      </div>
      <div class="panel-body">
        <table class="table table-bordered table-striped">
          <thead>
            <tr>
              <th class="text-center" style="width: 50px;">#</th>
              <th> Descripción </th>
              <th class="text-center" style="width: 15%;"> Cantidad</th>
              <!-- <th class="text-center" style="width: 15%;"> Total </th> -->
              <th class="text-center" style="width: 15%;"> Fecha </th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($sales as $sale) : ?>
              <tr>
                <td class="text-center"><?php echo count_id(); ?></td>
                <td><?php echo remove_junk($sale['name']); ?></td>
                <td class="text-center"><?php echo (int)$sale['total_sales']; ?></td>
                <!-- <td class="text-center"><?php echo remove_junk($sale['total_saleing_price']); ?></td> -->
                <td class="text-center"><?php echo date("m/Y", strtotime($sale['date'])); ?></td>
              </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
      </div>
      <ul class="nav nav-tabs nav-justified">
        <li class="active"><a href="monthly_sales.php">Salidas</a></li>
        <li><a href="monthly_compras.php">Compras</a></li>
      </ul>
    </div>
  </div>

</div>

<?php } include_once('layouts/footer.php'); ?>