<?php
  $page_title = 'Lista de ventas';
  require_once('includes/load.php');
  require_once('includes/conex.php');
  // Checkin What level user has permission to view this page

$query_ventas = "SELECT input_part, VentaDescripcion , ClienteId, date, VentaUsuario, SUM(ventaTotal) as total FROM sales GROUP BY input_part";
$ventas = mysqli_query($conex, $query_ventas) or die(mysqli_error($conex));
$row_ventas= mysqli_fetch_assoc($ventas); 

$modulo=13;
require_once('permiso.php');

include_once('layouts/header.php'); ?>
  <?php if($row_permiso['RolVer']==0) {echo "No tiene permiso ";} else { ?>
<div class="row">
  <div class="col-md-6">
   
  </div>
</div>
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading clearfix">
          <strong>
            <span class="glyphicon glyphicon-th"></span>
            <span>Ventas</span>
          </strong>
          <div class="pull-right">
            <a href="venta.php" class="btn btn-primary" <?php if($row_permiso['RolAgregar']==0){echo 'style="pointer-events: none"';} ?>>Agregar venta</a>
          </div>
        </div>
        <div class="panel-body">
          <table class="table table-bordered table-striped">
            <thead>
              <tr>
                <th style="width: 12%;"> Codigo Salida</th>
                <th> Descripción </th>
                <th><div align="center">Cliente</div></th>
                <th  style="width: 15%;"> <div align="right">Total </div></th>
                <th class="text-center" style="width: 15%;"> Fecha </th>
                <th class="text-center" style="width: 15%;">Usuario</th>
                <th class="text-center" style="width: 100px;"> Acciones </th>
             </tr>
            </thead>
           <tbody>
             <?php do { ?>
             <tr>
               <td><?php printf('%08d', $row_ventas['input_part']); ?></td>
               <td><?php echo $row_ventas['VentaDescripcion'] ?></td>
               <td>
                 <div align="center">
                   <?php 
			   	$query_cliente = "SELECT ClienteId, ClienteNombre FROM clientes WHERE ClienteId = '{$row_ventas['ClienteId']}'";
				$cliente = mysqli_query($conex, $query_cliente) or die(mysqli_error($conex));
				$row_cliente = mysqli_fetch_assoc($cliente);
			    echo $row_cliente['ClienteNombre'];   
			   ?>
               </div></td>
               <td><div align="right"><?php echo $row_ventas['total'] ?></div></td>
                <td  class="text-center"><?php echo $row_ventas['date'] ?></td>
                <td  class="text-center">
                <?php 
				$query_user = "SELECT username FROM users WHERE id ='{$row_ventas['VentaUsuario']}'";
				$user = mysqli_query($conex, $query_user) or die(mysqli_error($conex));
				$row_user = mysqli_fetch_assoc($user);				
			    echo $row_user['username']; 
				?>                </td>
               <td class="text-center">
                  <div class="btn-group">
                     <a href="venta_detalle.php?venta=<?php echo $row_ventas['input_part']; ?>" class="btn btn-warning btn-xs"  title="Ver" data-toggle="tooltip" <?php if($row_permiso['RolEditar']==0){echo 'style="pointer-events: none"';} ?>>
                       <span class="glyphicon glyphicon-edit"></span>                     </a>
                     <a href="reporte.php?venta=<?php echo $row_ventas['input_part']; ?>" class="btn btn-danger btn-xs"  title="Imprimir" data-toggle="tooltip" target="_blank" >
                       <span class="glyphicon glyphicon-print"></span>                     </a>                  </div>               </td>
                        <?php } while ($row_ventas = mysqli_fetch_assoc($ventas)); ?>
             </tr>
           </tbody>
         </table>
        </div>
      </div>
    </div>
  </div>
<?php } include_once('layouts/footer.php'); ?>
