<?php
  $page_title = 'Entrada diaria';
  require_once('includes/load.php');
  // Checkin What level user has permission to view this page
   page_require_level(3);
?>

<?php
$day= date('Y-m-d'.' '. '00:00:00');
$day1= date('Y-m-d'.' '. '23:59:59');
$shops = dailySales('shop',$day,$day1);
?>
<?php include_once('layouts/header.php'); ?>
<div class="row">
  <div class="col-md-6">
    <?php echo display_msg($msg); ?>
  </div>
</div>
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading clearfix">
          <strong>
            <span class="glyphicon glyphicon-th"></span>
            <span>Entrada diaria</span>
          </strong>
        </div>
        <div class="panel-body">
          <table class="table table-bordered table-striped">
            <thead>
              <tr>
                <th class="text-center" style="width: 50px;">#</th>
                <th> Descripción </th>
                <!-- <th class="text-center" style="width: 15%;"> Cantidad vendida</th> -->
                <th class="text-center" style="width: 15%;"> Total </th>
                <th class="text-center" style="width: 15%;"> Fecha </th>
             </tr>
            </thead>
           <tbody>
             <?php foreach ($shops as $shop): ?>
             <tr>
               <td class="text-center"><?php echo count_id();?></td>
               <td><?php echo remove_junk($shop['name']); ?></td>
               <!-- <td class="text-center"><?php echo (int)$shop['qty']; ?></td> -->
               <td class="text-center"><?php echo remove_junk($shop['total_saleing_price']); ?></td>
               <td class="text-center"><?php echo date("d/m/Y H:i:s", strtotime ($shop['date'])); ?></td>
             </tr>
             <?php endforeach;?>
           </tbody>
         </table>
        </div>
        <ul class="nav nav-tabs nav-justified">
  <li><a href="daily_sales.php">Ventas</a></li>
  <li class="active"><a href="daily_compras.php">Compras</a></li>
</ul>
      </div>
    </div>
  </div>

<?php include_once('layouts/footer.php'); ?>
