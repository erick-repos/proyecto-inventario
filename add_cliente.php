<?php
$page_title = 'Agregar Proveedor';
require_once('includes/load.php');
// Checkin What level user has permission to view this page
page_require_level(1);
?>
<?php
if (isset($_POST['add'])) {

  $req_fields = array('name', 'dni', 'address');
  validate_fields($req_fields);
  if (find_by_tableName_clientes('clientes', $_POST['name']) === false) {
    $session->msg('d', '<b>Error!</b> Proveedor ya existe en la base de datos');
    redirect('add_cliente.php', false);
  }
  if (empty($errors)) {
    $name = remove_junk($db->escape($_POST['name']));
    $dni = remove_junk($db->escape($_POST['dni']));
    $direccion = remove_junk($db->escape($_POST['address']));

    $query  = "INSERT INTO clientes (";
    $query .= "ClienteNombre,ClienteDNI,ClienteDireccion,ClienteStatus";
    $query .= ") VALUES (";
    $query .= " '{$name}', '{$dni}','{$direccion}', 1";
    $query .= ")";
    if ($db->query($query)) {
      //sucess
      $session->msg('s', "Cliente creado Exitosamente! ");
      redirect('clientes.php', false);
    } else {
      //failed
      $session->msg('d', 'Lamentablemente no se pudo crear el Cliente!');
      redirect('add_cliente.php', false);
    }
  } else {
    $session->msg("d", $errors);
    redirect('add_cliente.php', false);
  }
}
?>
<?php include_once('layouts/header.php'); ?>
<div class="login-page">
  <div class="text-center">
    <h3>Agregar nuevo Cliente</h3>
  </div>
  <?php echo display_msg($msg); ?>
  <form method="post" action="add_cliente.php" class="clearfix">
    <div class="form-group">
      <label for="name" class="control-label">Nombre de Cliente</label>
      <input type="name" class="form-control" name="name" required>
    </div>
    <div class="form-group">
      <label for="level" class="control-label">Dni</label>
      <input type="text" class="form-control" name="dni">
    </div>
    <div class="form-group">
      <label for="direccion_proveedor" class="control-label">Direccion</label>
      <input type="text" class="form-control" name="address">
    </div>
    <!-- <div class="form-group">
          <label for="status">Estado</label>
            <select class="form-control" name="status">
              <option value="1">Activo</option>
              <option value="0">Inactivo</option>
            </select>
        </div> -->
    <div class="form-group clearfix">
      <button type="submit" name="add" class="btn btn-info">Guardar</button>
    </div>
  </form>
</div>

<?php include_once('layouts/footer.php'); ?>