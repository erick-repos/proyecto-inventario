<?php
  $page_title = 'Lista de categorías';
  require_once('includes/load.php');
  // Checkin What level user has permission to view this page
  page_require_level(1);
  
  $all_manufacturers = find_all('manufacturer')
?>
<?php
 if(isset($_POST['add_manu'])){
   $req_field = array('manufacturer-name');
   validate_fields($req_field);
   if(find_by_tableName('manufacturer',$_POST['manufacturer-name']) === false ){
    $session->msg('d','<b>Error!</b> Fabricante ya existe en la base de datos');
    redirect('manufacturer.php', false);
  }
   $manu_name = remove_junk($db->escape($_POST['manufacturer-name']));
   if(empty($errors)){
      $sql  = "INSERT INTO manufacturer (name, status)";
      $sql .= " VALUES ('{$manu_name}','1')";
      if($db->query($sql)){
        $session->msg("s", "Fabricante agregado exitosamente.");
        redirect('manufacturer.php',false);
      } else {
        $session->msg("d", "Lo siento, registro falló");
        redirect('manufacturer.php',false);
      }
   } else {
     $session->msg("d", $errors);
     redirect('manufacturer.php',false);
   }
 }
 
$modulo=3;
require_once('permiso.php')
 
 
?>
<?php include_once('layouts/header.php'); ?>

  <div class="row">
     <div class="col-md-12">
       <?php echo display_msg($msg); ?>
     </div>
  </div>
  <?php if($row_permiso['RolVer']==0) {echo "No tiene permiso ";} else { ?>
   <div class="row">
    <div class="col-md-5">
      <div class="panel panel-default">
        <div class="panel-heading">
          <strong>
            <span class="glyphicon glyphicon-th"></span>
            <span>Agregar Fabricante</span>
         </strong>
        </div>
        <div class="panel-body">
          <form method="post" action="manufacturer.php">
            <div class="form-group">
                <input type="text" class="form-control" name="manufacturer-name" placeholder="Nombre del Fabricante" required>
            </div>
            <button type="submit" name="add_manu" class="btn btn-primary" <?php if($row_permiso['RolAgregar']==0){echo 'style="pointer-events: none"';} ?>>Agregar fabricante</button>
        </form>
        </div>
      </div>
    </div>
    <div class="col-md-7">
    <div class="panel panel-default">
      <div class="panel-heading">
        <strong>
          <span class="glyphicon glyphicon-th"></span>
          <span>Lista de Fabricantes</span>
       </strong>
      </div>
        <div class="panel-body">
          <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th class="text-center" style="width: 50px;">#</th>
                    <th>Fabricante</th>
                    <th>Status</th>
                    <th class="text-center" style="width: 100px;">Acciones</th>
                    
                </tr>
            </thead>
            <tbody>
              <?php foreach ($all_manufacturers as $manu):?>
                <tr>
                    <td class="text-center"><?php echo count_id();?></td>
                    <td><?php echo remove_junk(ucfirst($manu['name'])); ?></td>
                    <td class="text-center">
                    <?php if($manu['status'] === '1'): ?>
                      <span class="label label-success"><?php echo "Activo"; ?></span>
                    <?php else: ?>
                      <span class="label label-danger"><?php echo "Inactivo"; ?></span>
                    <?php endif;?>
                    </td>
                    <!-- <td><?php echo remove_junk(ucfirst($cat['status'])); ?></td> -->
                    <td class="text-center">
                      <div class="btn-group">
                        <a href="edit_manufacturer.php?id=<?php echo (int)$manu['id'];?>"  <?php  if($row_permiso['RolEditar']==0){echo 'style="pointer-events: none;"';} ?>  class="btn btn-xs btn-warning" data-toggle="tooltip" title="Editar">
                          <span class="glyphicon glyphicon-edit"></span>
                        </a>
                        <!-- <a href="delete_categorie.php?id=<?php echo (int)$manu['id'];?>"  class="btn btn-xs btn-danger" data-toggle="tooltip" title="Eliminar">
                          <span class="glyphicon glyphicon-trash"></span>
                        </a> -->
                      </div>
                    </td>

                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
       </div>
    </div>
    </div>
   </div>
   <?php } ?>
  </div>
  <?php include_once('layouts/footer.php'); ?>
