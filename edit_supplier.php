<?php
  $page_title = 'Editar Proveedor';
  require_once('includes/load.php');
  // Checkin What level user has permission to view this page
?>
<?php
  $supplier = find_by_id('proveedores',(int)$_GET['id']);
  if(!$supplier){
    $session->msg("d","Missing Group id.");
    redirect('edit_supplier.php');
  }
?>
<?php
  if(isset($_POST['update'])){
   $req_fields = array('status');
   validate_fields($req_fields);
   if(empty($errors)){
           $name = remove_junk($db->escape($_POST['name']));
           $phone = remove_junk($db->escape($_POST['phone']));
           $email = remove_junk($db->escape($_POST['email']));
         $status = remove_junk($db->escape($_POST['status']));
         $address = remove_junk($db->escape($_POST['address']));
        $query  = "UPDATE proveedores SET ";
        $query .= "name='{$name}',phone='{$phone}',email='{$email}',status='{$status}',address='{$address}'";
        $query .= "WHERE ID='{$db->escape($supplier['id'])}'";
        $result = $db->query($query);
         if($result && $db->affected_rows() === 1){
          //sucess
          $session->msg('s',"Proveedor se ha actualizado Exitosamente! ");
          redirect('supplier.php?id='.(int)$supplier['id'], false);
        } else {
          //failed
          $session->msg('d','Lamentablemente no se ha actualizado el Proveedor!');
          redirect('edit_supplier.php?id='.(int)$supplier['id'], false);
        }
   } else {
     $session->msg("d", $errors);
    redirect('edit_supplier.php?id='.(int)$supplier['id'], false);
   }
 }
?>
<?php include_once('layouts/header.php'); ?>
<div class="login-page">
    <div class="text-center">
       <h3>Editar Proveedor</h3>
     </div>
     <?php echo display_msg($msg); ?>
      <form method="post" action="edit_supplier.php?id=<?php echo (int)$supplier['id'];?>" class="clearfix">
        <div class="form-group">
              <label for="name" class="control-label">Nombre</label>
              <input type="name" class="form-control" name="name"  value="<?php echo remove_junk(ucwords($supplier['name'])); ?>">
        </div>
        <div class="form-group">
              <label for="level" class="control-label">Telefono</label>
              <input type="tel" class="form-control" name="phone"  value="<?php echo (int)$supplier['phone']; ?>">
        </div>
        <div class="form-group">
              <label for="level" class="control-label">Email</label>
              <input type="email" class="form-control" name="email"  value="<?php echo remove_junk(ucwords($supplier['email'])); ?>">
        </div>
        <div class="form-group">
              <label for="level" class="control-label">Dirección</label>
              <input type="text" class="form-control" name="address"  value="<?php echo remove_junk(ucwords($supplier['address'])); ?>">
        </div>
        <div class="form-group">
          <label for="status">Estado</label>
              <select class="form-control" name="status">
                <option <?php if($supplier['status'] === '1') echo 'selected="selected"';?> value="1"> Activo </option>
                <option <?php if($supplier['status'] === '0') echo 'selected="selected"';?> value="0">Inactivo</option>
               <!--  <option <?php if($supplier['status'] === '0') echo 'selected="selected"';?> value="0">Inactivo</option> -->
              </select>
        </div>
        <div class="form-group clearfix">
                <button type="submit" name="update" class="btn btn-info">Actualizar</button>
        </div>
    </form>
</div>

<?php include_once('layouts/footer.php'); ?>
