<ul>
  <li>
    <a href="admin.php">
      <i class="glyphicon glyphicon-home"></i>
      <span>Panel de control</span>
    </a>
  </li>
  <li>
    <a href="#" class="submenu-toggle">
      <i class="glyphicon glyphicon-user"></i>
      <span>Accesos</span>
    </a>
    <ul class="nav submenu">
      <li><a href="group.php">Administrar grupos</a> </li>
      <li><a href="users.php">Administrar usuarios</a> </li>
    </ul>
  </li>
  <li>
    <a href="manufacturer.php">
      <i class="glyphicon glyphicon-tag"></i>
      <span>Fabricantes</span>
    </a>
  </li>
  <li>
    <a href="supplier.php">
      <i class="glyphicon glyphicon-retweet"></i>
      <span>Proveedores</span>
    </a>
  </li>
  <li>
    <a href="clientes.php">
      <i class="fa fa-users"></i>
      <span>Clientes</span>
    </a>
  </li>
  <li>
    <a href="categorie.php">
      <i class="glyphicon glyphicon-indent-left"></i>
      <span>Categorías</span>
    </a>
  </li>
  <li>
    <a href="#" class="submenu-toggle">
      <i class="glyphicon glyphicon-th-large"></i>
      <span>Productos</span>
    </a>
    <ul class="nav submenu">
      <li><a href="product.php">Administrar productos</a> </li>
      <li><a href="add_product.php">Agregar productos</a> </li>
      <li><a href="media.php">Media</a> </li>
    </ul>
  </li>
  <li>
    <a href="#" class="submenu-toggle">
      <i class="fa fa-truck"></i>
      <span>Entradas</span>
    </a>
    <ul class="nav submenu">
      <li><a href="compra.php">Agregar Compra</a> </li>
      <li><a href="registro_compras.php">Historial de Compras</a> </li>
    </ul>
  </li>
  <li>
    <a href="#" class="submenu-toggle">
      <i class="glyphicon glyphicon-shopping-cart"></i>
      <span>Salidas</span>
    </a>
    <ul class="nav submenu">
      <li><a href="venta.php">Agregar Venta</a> </li>
      <li><a href="registro_ventas.php">Historial de Ventas</a> </li>

    </ul>
  </li>
  <li>
    <a href="#" class="submenu-toggle">
      <i class="glyphicon glyphicon-signal"></i>
      <span>Reportes</span>
    </a>
    <ul class="nav submenu">
      <li><a href="sales_report.php">Movimientos por fecha </a></li>
      <li><a href="monthly_sales.php">Movimientos mensuales</a></li>
      <li><a href="daily_sales.php">Movimientos diarios</a> </li>
      <li><a href="kardex.php">Kardex</a> </li>
    </ul>
  </li>
</ul>