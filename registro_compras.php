<?php
$page_title = 'Lista de compras';

require_once('includes/load.php');
require_once('includes/conex.php');
// Checkin What level user has permission to view this page

$query_compras = "SELECT input_part, CompraDescripcion, MovEntradaId , CompraDoc , ProveedorId, date, CompraUsuario, SUM(CompraTotal) as total FROM shop GROUP BY input_part";
$compras = mysqli_query($conex, $query_compras) or die(mysqli_error($conex));
$row_compras = mysqli_fetch_assoc($compras);

$modulo=11;
require_once('permiso.php');

include_once('layouts/header.php'); ?>
  <?php if($row_permiso['RolVer']==0) {echo "No tiene permiso ";} else { ?>
<div class="row">
  <div class="col-md-6">

  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading clearfix">
        <strong>
          <span class="glyphicon glyphicon-th"></span>
          <span>Compras</span>
        </strong>
        <div class="pull-right">
          <a href="compra.php" class="btn btn-primary" <?php if($row_permiso['RolAgregar']==0){echo 'style="pointer-events: none"';} ?>>Agregar compra</a>
        </div>
      </div>
      <div class="panel-body">
        <table class="table table-bordered table-striped">
          <thead>
            <tr>
              <th style="width: 10%;"> Cod. Entrada</th>
              <th style="width: 10%;">Tipo de Mov</th>
              <th style="width: 30%;"> Descripción </th>
              <th> Doc Compra </th>
              <th>
                <div align="center">Proveedor</div>
              </th>
              <th class="text-center">Total </th>
              <th class="text-center"> Fecha </th>
              <th class="text-center"">Usuario</th>
                <th class=" text-center"> Acciones </th>
            </tr>
          </thead>
          <tbody>
            <?php do { ?>
              <tr>
                <td><?php printf('%08d', $row_compras['input_part']); ?></td>
                <!-- <td><?php echo $query_movimientos['name'] ?></td> -->
                <td>
                  <div align="center">
                    <?php
                    $row_movimientos = find_by_id('moventrada', $row_compras['MovEntradaId']);
                    echo $row_movimientos['name'];
                    ?>
                  </div>
                </td>
                <td><?php echo $row_compras['CompraDescripcion'] ?></td>
                <td><?php echo $row_compras['CompraDoc'] ?></td>
                <td>
                  <div align="center">
                    <?php
                    $row_proveedor = find_by_id('proveedores', $row_compras['ProveedorId']);
                    echo $row_proveedor['name'];
                    ?>
                  </div>
                </td>
                <td>
                  <div align="right"><?php echo $row_compras['total'] ?></div>
                </td>
                <td class="text-center"><?php echo $row_compras['date'] ?></td>
                <td class="text-center">
                  <?php
                  $query_user = "SELECT username FROM users WHERE id ='{$row_compras['CompraUsuario']}'";
                  $user = mysqli_query($conex, $query_user) or die(mysqli_error($conex));
                  $row_user = mysqli_fetch_assoc($user);
                  echo $row_user['username'];
                  ?> </td>
                <td class="text-center">
                  <div class="btn-group">
                    <a href="compra_detalle.php?compra=<?php echo $row_compras['input_part']; ?>" class="btn btn-warning btn-xs" title="Ver" data-toggle="tooltip" <?php if($row_permiso['RolEditar']==0){echo 'style="pointer-events: none"';} ?>>
                      <span class="glyphicon glyphicon-edit"></span> </a>
                    <a href="reporte.php?compra=<?php echo $row_compras['input_part']; ?>" class="btn btn-danger btn-xs" title="Imprimir" data-toggle="tooltip" target="_blank">
                      <span class="glyphicon glyphicon-print"></span> </a>
                  </div>
                </td>
              <?php } while ($row_compras = mysqli_fetch_assoc($compras)); ?>
              </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<?php } include_once('layouts/footer.php'); ?>