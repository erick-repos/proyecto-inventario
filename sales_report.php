<?php
$page_title = 'Reporte de Salidas';
require_once('includes/load.php');
$modulo=14;
require_once('permiso.php');
// Checkin What level user has permission to view this page
page_require_level(3);
?>
<?php include_once('layouts/header.php'); ?>
  <?php if($row_permiso['RolVer']==0) {echo "No tiene permiso ";} else { ?>
<div class="row">
  <div class="col-md-6">
    <?php echo display_msg($msg); ?>
  </div>
</div>

<div class="row">
  <div class="col-md-6">
  <ul class="nav nav-tabs nav-justified">
  <li class="active"><a href="#">Salidas</a></li>
  <li><a href="compras_report.php">Entradas</a></li>
</ul>
    <div class="panel">
      <div class="panel-body">
      <div class="panel-report">
        <strong>
          <span class="glyphicon glyphicon-th"></span>
          <span>Reporte de Salidas por Fecha</span>
        </strong>
      </div>
        <form class="clearfix" method="post" action="sale_report_process.php">
          <div class="form-group">
            <label class="form-label">Rango de fechas</label>
            <div class="input-group">
              <input type="text" class="datepicker form-control" name="start-date" placeholder="From">
              <span class="input-group-addon"><i class="glyphicon glyphicon-menu-right"></i></span>
              <input type="text" class="datepicker form-control" name="end-date" placeholder="To">
            </div>
          </div>
          <div class="form-group">
            <button type="submit" name="submit" class="btn btn-primary">Generar Reporte</button>
          </div>
      </div>


    </div>

  </div>
  <?php } include_once('layouts/footer.php'); ?>