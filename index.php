<?php
  ob_start();
  require_once('includes/load.php');
  if($session->isUserLoggedIn(true)) { redirect('admin.php', false);}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="shortcut icon" href="uploads/images/favicon.ico" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css" />
    <link rel="stylesheet" href="libs/css/login.css" />
  <title><?php if (!empty($page_title))
           echo remove_junk($page_title);
            elseif(!empty($user))
           echo ucfirst($user['name']);
            else echo "Sistema simple de inventario";?></title>
  <link rel="icon" type="image/png" href="uploads/images/logo.png">
</head>
<body>
<div class="col-min-4 login container row ">
    </div>
      <div class="login-page">

    <div class="text-center">
       <h1>Bienvenido</h1>
       <p>Regístrese para iniciar su sesión</p>
          </div>
          <?php echo display_msg($msg); ?>
            <form method="post" action="auth_v2.php" class="clearfix">
              <div class="form-group">
                    <label for="username" class="control-label">Usuario</label>
                    <input type="name" class="form-control" name="username" placeholder="Usuario">
              </div>
              <div class="form-group">
                  <label for="Password" class="control-label">Password</label>
                  <input type="password" name= "password" class="form-control" placeholder="Password">
              </div>
        <div class="form-group">
                <button type="submit" class="btn btn-info  pull-center">Entrar</button>
        </div>
    </form>
</div>
</body>
</html>
  

