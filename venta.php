<?php
$page_title = 'Agregar venta';
require_once('includes/load.php');
require_once('includes/conex.php');
page_require_level(3);

if (!function_exists("GetSQLValueString")) {
    function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
    {
		global $conex;
      if (PHP_VERSION < 6) {
        $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
      }
      $theValue = function_exists("mysqli_real_escape_string") ? mysqli_real_escape_string($conex, $theValue) : mysqli_escape_string($conex, $theValue);
    
      switch ($theType) {
        case "text":
          $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
          break;    
        case "long":
        case "int":
          $theValue = ($theValue != "") ? intval($theValue) : "NULL";
          break;
        case "double":
          $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
          break;
        case "date":
          $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
          break;
        case "defined":
          $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
          break;
      }
      return $theValue;
    }
}
date_default_timezone_set('America/Lima');
$fecha=date("Y-m-d");
$hora= date('H:i:s');
$hoy= $fecha." ".$hora;

$numeroventa=0;
$total = 0;
$mensaje="";

$all_movimientos = find_all('movsalida');

$query_cliente = "SELECT ClienteId, ClienteNombre FROM clientes where CLienteStatus ='1'";
$cliente = mysqli_query($conex, $query_cliente) or die(mysqli_error($conex));
$row_cliente = mysqli_fetch_assoc($cliente);

$query_productos = "SELECT id, product_id, qty, price, ClienteId, VentaTotal FROM sales WHERE VentaEstado = 0";
$productos = mysqli_query($conex, $query_productos) or die(mysqli_error($conex));
$row_productos= mysqli_fetch_assoc($productos); 
$totalRows_productos = mysqli_num_rows($productos);

$query_descripcion = "SELECT VentaDescripcion, VentaDoc FROM sales ORDER BY id DESC LIMIT 1";
$descripcion = mysqli_query($conex, $query_descripcion) or die(mysqli_error($conex));
$row_descripcion= mysqli_fetch_assoc($descripcion); 

if(isset($_POST['agregar']))
{

	$query_ultimaventa = "SELECT id, input_part, VentaEstado FROM sales ORDER BY id DESC LIMIT 1 ";
	$ultimaventa = mysqli_query($conex, $query_ultimaventa) or die(mysqli_error($conex));
	$row_ultimaventa= mysqli_fetch_assoc($ultimaventa); 
	$totalRows_ultimaventa = mysqli_num_rows($ultimaventa); 
	echo $totalRows_ultimaventa;
	if($totalRows_ultimaventa==0)
	{
	
		$numeroventa = 1;
	}
	else
	{	
		if( $row_ultimaventa['VentaEstado']==0)
		{
		
			$numeroventa = $row_ultimaventa['input_part'];
		}
		else
		{
		
			$numeroventa = $row_ultimaventa['input_part'] + 1;
		}
	}
	

	$query_buscaproducto = "SELECT  id, name, sale_price, quantity FROM products WHERE name ='{$_POST['title']}'";
	$buscaproducto = mysqli_query($conex, $query_buscaproducto) or die(mysqli_error($conex));
	$row_buscaproducto= mysqli_fetch_assoc($buscaproducto); 
	$VentaTotal = $_POST['Cantidad'] * $row_buscaproducto['sale_price'];
	
	$stock = $row_buscaproducto['quantity'] - $_POST['Cantidad'];
	if($stock>=0)
	{	
		$stock = $row_buscaproducto['quantity'] - $_POST['Cantidad'];	
		
		  
		$insertSQL = sprintf("INSERT INTO sales ( product_id, qty, price, ClienteId, VentaDescripcion, MovSalidaId, VentaDoc, VentaTotal, input_part,  date, VentaUsuario, ProductoStock, VentaPU) 
    VALUES (  %s, %s, %s,%s, %s, %s, %s, %s, '{$numeroventa}', '{$hoy}', '{$_SESSION['user_id']}', '{$stock}', '{$row_buscaproducto['sale_price']}')",
								   GetSQLValueString($row_buscaproducto['id'], "int"),
								   GetSQLValueString($_POST['Cantidad'], "int"),
								   GetSQLValueString($row_buscaproducto['sale_price'], "int"),		   
								   GetSQLValueString($_POST['ClienteId'], "int"),
								   GetSQLValueString($_POST['VentaDescripcion'], "text"),
                   GetSQLValueString($_POST['VentaMovimiento'], "text"),
                   GetSQLValueString($_POST['VentaDoc'], "text"), 
								   GetSQLValueString($VentaTotal, "int"));	
					 
		   $Result1 = mysqli_query( $conex, $insertSQL) or die(mysqli_error($conex));
		   
			
		   
			$updateSQL = "UPDATE products SET quantity='{$stock}'  WHERE id='{$row_buscaproducto['id']}'";
			mysqli_select_db( $conex, $database_conex);
			$Result1 = mysqli_query( $conex, $updateSQL) or die(mysqli_error($conex));
			
			$updateGoTo = "venta.php?ClienteId=".$_POST['ClienteId']; 
 			 header("Location:".$updateGoTo);
			
	}	
	else
	{
			 $updateGoTo = "venta.php?m=1&ClienteId=".$_POST['ClienteId']; 
 			 header("Location:".$updateGoTo);
	
	}
   

  
}

if(isset($_GET['id']))
{
	$query_buscaproducto = "SELECT  id, name, sale_price, quantity FROM products WHERE id ='{$_GET['ProductoId']}'";
	$buscaproducto = mysqli_query($conex, $query_buscaproducto) or die(mysqli_error($conex));
	$row_buscaproducto= mysqli_fetch_assoc($buscaproducto); 
	
	$stockR = $row_buscaproducto['quantity'] + $_GET['Cantidad'];
	
	$updateSQL = "UPDATE products SET quantity='{$stockR}'  WHERE id='{$_GET['ProductoId']}'";
	echo $updateSQL;
    mysqli_select_db( $conex, $database_conex);
  	$Result1 = mysqli_query( $conex, $updateSQL) or die(mysqli_error($conex));

  $deleteSQL = "DELETE FROM sales WHERE id='{$_GET['id']}'"; 
  $Result1 = mysqli_query($conex, $deleteSQL) or die(mysqli_error());
   
 
   if($totalRows_productos==1)
   {
  
    $updateGoTo = "venta.php";
   }
   else
   {
   	$updateGoTo = "venta.php?ClienteId=".$_GET['ClienteId'];
   }
   
  header("Location:".$updateGoTo);
}

if(isset($_GET['total']))
{
	$updateSQL = "UPDATE sales SET VentaEstado=1  WHERE VentaEstado=0";
    mysqli_select_db( $conex, $database_conex);
  	$Result1 = mysqli_query( $conex, $updateSQL) or die(mysqli_error($conex));
	
	$query_ultimaventa = "SELECT input_part FROM sales ORDER BY id DESC LIMIT 1 ";
	$ultimaventa = mysqli_query($conex, $query_ultimaventa) or die(mysqli_error($conex));
	$row_ultimaventa= mysqli_fetch_assoc($ultimaventa); 
	$updateGoTo = "venta_detalle.php?venta=".$row_ultimaventa['input_part']; 
  	header("Location:".$updateGoTo);
}

if(isset($_GET['veliminaventa']))
{
	  $deleteSQL = "DELETE FROM sales WHERE VentaEstado=0"; 
  $Result1 = mysqli_query($conex, $deleteSQL) or die(mysqli_error($conex));
  
  $updateGoTo = "venta.php"; 
  header("Location:".$updateGoTo);
}
$modulo=12;
require_once('permiso.php');
?>
<?php include_once('layouts/header.php'); ?>
  <?php if($row_permiso['RolVer']==0) {echo "No tiene permiso ";} else { ?>
<form action="venta.php" method="post" name="form1" id="form1" autocomplete="off">
  <div class="row">
    <div class="col-md-6">
      <div class="form-group">
        <div class="input-group">
          <span class="input-group-btn">
            <span class="btn btn-primary">Cliente&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
          </span>
          <select name="ClienteId" id="ClienteId" class="form-control" required>
            <option></option>
            <?php do { ?>
              <option value="<?php echo $row_cliente['ClienteId'] ?>" <?php
                                                                      if (isset($_GET['ClienteId'])) {
                                                                        if ($row_cliente['ClienteId'] == $_GET['ClienteId']) {
                                                                          echo "selected";
                                                                        }
                                                                      }
                                                                      ?>>
                <?php echo $row_cliente['ClienteNombre'] ?></option>
            <?php } while ($row_cliente = mysqli_fetch_assoc($cliente)); ?>
          </select>
        </div>
        <br>
        <div class="input-group">
          <span class="input-group-btn">
            <button type="submit" class="btn btn-primary">Documento</button>
          </span>
          <input name="VentaDoc" type="text" placeholder="Ingresa Documento de Venta" class="form-control" id="VentaDoc" value="<?php if (isset($_GET['ProveedorId'])) {
                                                                                                          echo $row_descripcion['VentaDoc'];
                                                                                                        } ?>" required>
        </div>
        <br>
        <div class="input-group">
          <span class="input-group-btn">
            <button type="submit" class="btn btn-primary">Búsqueda</button>
          </span>
          <input type="text" id="sug_input" class="form-control" name="title" placeholder="Buscar por el nombre del producto" required>
        </div>
        <div id="result" class="list-group" style="margin: auto;"></div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group ">
        <div class="input-group">
          <span class="input-group-btn">
            <button type="submit" class="btn btn-primary">Cantidad&nbsp;&nbsp;&nbsp;&nbsp;</button>
          </span>
          <input name="Cantidad" type="number" class="form-control" id="Cantidad" required>
        </div>
        <br>
        <div class="input-group">
          <span class="input-group-btn">
            <span class="btn btn-primary">Movimiento</span>
          </span>
          <select class="form-control" name="VentaMovimiento" id="VentaMovimiento" required>
                    <option value="">Selecciona un Tipo de  Movieminto</option>
                    <?php foreach ($all_movimientos as $mov) : ?>
                      <option value="<?php echo (int)$mov['id'] ?>">
                        <?php echo $mov['name'] ?></option>
                    <?php endforeach; ?> 
                  </select>
          
        </div>
        <br>
        <div class="input-group">
          <span class="input-group-btn">
            <button type="submit" class="btn btn-primary">Descripción</button>
          </span>
          <input name="VentaDescripcion" type="text" class="form-control" id="VentaDescripcion" value="<?php if (isset($_GET['ClienteId'])) {
                                                                                                          echo $row_descripcion['VentaDescripcion'];
                                                                                                        } ?>" required>
        </div>
      </div>


      <?php if (isset($_GET['m'])) { ?>
        <span class="btn btn-danger">No hay suficiente Stock</span>
      <?php }

      if (!isset($_GET['ClienteId']) && $totalRows_productos > 0) { ?>
        <a href="venta.php?veliminaventa=1" class="btn btn-danger">Elimine productos antes de <br>hacer una nueva venta</a>
      <?php } ?>
    </div>
  </div>
  <div class="btn-add">
  <a href="registro_ventas.php" class="pull-right btn btn-primary">Ver Ventas</a>
        <input name="agregar" type="submit" class="pull-right btn btn-danger" id="agregar" value="Agregar" <?php if($row_permiso['RolAgregar']==0){echo 'style="pointer-events: none"';} ?>>
  </div>
   
</form>

<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading clearfix">
        <strong>
          <span class="glyphicon glyphicon-th"></span>
          <span>Productos</span>
        </strong>
      </div>
      <div class="panel-body">
        <form method="post" action="add_sale.php">
       <table class="table table-bordered">
            <thead>
              <th colspan="3"> Producto </th>
              <!-- <th> Precio </th> -->
              <th> <div align="center">Cantidad </div></th>
              <!-- <th> <div align="right">Total </div></th> -->
              <th> <div align="center">Acción </div></th>
            </thead>
            <tbody id="product_info"> </tbody>
            <?php 		
			if($totalRows_productos<>0) {
			do { 
						$query_productolista = "SELECT name, sale_price FROM products WHERE id = '{$row_productos['product_id']}'";
            $productolista = mysqli_query($conex, $query_productolista) or die(mysqli_error($conex));
            $row_productolista= mysqli_fetch_assoc($productolista); 
			?>
              <tr>
              <td colspan="3"> <?php echo $row_productolista['name']; ?> </td>
              <!-- <td> <?php echo $row_productolista['sale_price']; ?> </td> -->
              <td> <div align="center"><?php echo $row_productos['qty']; ?> </div></td>
              <!-- <td> 
              <div align="right">
              <?php
			          //  $total = $row_productolista['sale_price'] * $row_productos['qty'] + $total; 
                 $total = $row_productos['qty'] + $total;
			           echo number_format($row_productolista['sale_price'] * $row_productos['qty'], 2, '.',','); ?> 
                  </div></td> -->
              <td align="center">
           <?php  
           if(!isset($_GET['ClienteId']) && $totalRows_productos>0) {} else{ ?>
               <a href="venta.php?id=<?php echo $row_productos['id']; ?>&ClienteId=<?php echo $_GET['ClienteId'] ?>&Cantidad=<?php echo $row_productos['qty'] ?>&ProductoId=<?php echo $row_productos['product_id'] ?>" class="btn btn-danger btn-xs"  title="Eliminar" data-toggle="tooltip"> <span class="glyphicon glyphicon-trash"></span>                   </a>
            <?php } ?>
           </td>
           </tr>           
   <?php } while ($row_productos = mysqli_fetch_assoc($productos)); ?>
            <tr>
              <td colspan="3"> <div align="right"><span style="font-weight: bold">TOTAL</span></div></td>
              <td><div align="center"><?php echo number_format($total, 0, '.',','); ?>  </div></td>
              <td align="center">
               <?php  if(!isset($_GET['ClienteId']) && $totalRows_productos>0) {} else{ ?>
              <a href="venta.php?total=<?php echo $total; ?>" class="btn btn-primary" >Cerrar Venta</a>
                <?php } ?>              </td>
              </tr>
              <?php } ?>     
          </table>
        </form>
      </div>
    </div>
  </div>

</div>

<?php } include_once('layouts/footer.php'); ?>

<?php
mysqli_free_result($cliente);
mysqli_free_result($productos);
mysqli_free_result($descripcion);
@ mysqli_free_result($ultimaventa);
@ mysqli_free_result($buscaproducto);

?>
