<?php
$page_title = 'Editar producto';
require_once('includes/load.php');
// Checkin What level user has permission to view this page
page_require_level(2);
?>
<?php

$product = find_by_id('products', (int)$_GET['id']);
$manufacturer = find_by_id('manufacturer', (int)$product['manu_id']);
$categorie = find_by_id('categories', (int)$product['categorie_id']);
$sales_date = find_by_max_date_id('sales',(int)$_GET['id']);
$shop_date = find_by_max_date_id('shop',(int)$_GET['id']);
$und_medida = find_by_id('unidadmedida', (int)$product['pmedida']);


if (!$product) {
  $session->msg("d", "Missing product id.");
  redirect('product.php');
}
$media = find_by_id('media', (int)$product['media_id']);
?>
<?php
if (isset($_POST['product'])) {
  $req_fields = array('product-status');
  validate_fields($req_fields);
  if (empty($errors)) {
   if((!empty($sales_date) && (int)$sales_date['old_date'] >= 10) && (!empty($shop_date) && (int)$shop_date['old_date']) >= 10) {
      $p_status  = remove_junk($db->escape($_POST['product-status']));
      $query   = "UPDATE products SET";
      $query  .= " status ='{$p_status}'";
      $query  .= " WHERE id ='{$product['id']}'";
      $result = $db->query($query);
    }
    if(empty($sales_date) && empty($shop_date)){
      $p_status  = remove_junk($db->escape($_POST['product-status']));
      $query   = "UPDATE products SET";
      $query  .= " status ='{$p_status}'";
      $query  .= " WHERE id ='{$product['id']}'";
      $result = $db->query($query);
    }
      if ($result && $db->affected_rows() === 1) {
        $session->msg('s', "Producto ha sido actualizado. ");
        redirect('product.php', false);
      }
      if (empty($result)) {
        $session->msg('d', ' Lo siento, el producto aun cuenta con movimientos dentro de la fecha .');
        redirect('edit_product.php?id=' . $product['id'], false);
      }
      else{
      $session->msg('d', ' Lo siento, la actualización fallo o no hay cambios en el estado del producto');
      redirect('edit_product.php?id=' . $product['id'], false);
    }
    
  } else {
    $session->msg("d", $errors);
    redirect('edit_product.php?id=' . $product['id'], false);
  }
}

?>
<?php include_once('layouts/header.php'); ?>
<div class="row">
  <div class="col-md-12">
    <?php echo display_msg($msg); ?>
  </div>
</div>
<div class="row">
  <div class="panel panel-default">
    <div class="panel-heading clearfix">
    <div class="pull-right">
           <a href="product.php" class="btn btn-primary">Ver Productos</a>
         </div>
      <strong>
        <span class="glyphicon glyphicon-th"></span>
        <span>Editar producto</span>
      </strong>
    </div>
    <div class="panel-body">
      <div class="col-md-7">
        <form method="post" action="edit_product.php?id=<?php echo (int)$product['id'] ?>">
          <div class="col-md-6">
            <div class="form-group">
           <label class="label-center">Código</label>
              <div class="input-group codigo-title">
                <span class="input-group-addon">
                  <i class="glyphicon glyphicon-barcode"></i>
                </span>
                <input type="text" class="form-control " readonly name="codigo-title" value="<?php echo remove_junk($product['code']); ?>">
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
          <label class="label-center">Fabricante</label>
              <div class="input-group codigo-title">
                <span class="input-group-addon">
                  <i class="fas fa-industry"></i>
                </span>
                <input type="text" class="form-control product-manufact" readonly name="codigo-manufact" value="<?php echo remove_junk($manufacturer['name']); ?>">
              </div>
            </div>
          </div>
          <div class="form-group">
          <label for="qty">Descripción</label>
            <div class="input-group">
              <span class="input-group-addon">
                <i class="glyphicon glyphicon-th-large"></i>
              </span>
              <input type="text" class="form-control" readonly name="product-title" value="<?php echo remove_junk($product['name']); ?>">
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
              <label for="qty">Categorías</label>
                  <input type="text" class="form-control" readonly name="codigo-categorie" value="<?php echo remove_junk($categorie['name']); ?>">
                </div>
              </div>
              <div class="col-md-6">
              <label for="qty">Unidad de Medida</label>
              <input type="text" class="form-control" readonly name="codigo-umedida" value="<?php echo remove_junk($und_medida['name']); ?>">
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
            <div class="col-md-6">
              <label for="qty">Estado</label>
                <select class="form-control" name="product-status" readonly>
                  <option value="">Selecciona estado</option>
                  <option <?php if ($product['status'] === '1') echo 'selected="selected"'; ?>value="1">Activo</option>
                  <option <?php if ($product['status'] === '0') echo 'selected="selected"'; ?> value="0">Inactivo</option>
                </select>
              </div>
              <!-- <div class="col-md-4">
                <div class="form-group">
                  <label for="qty">Stock</label>
                  <div class="input-group">
                    <span class="input-group-addon">
                      <i class="glyphicon glyphicon-shopping-cart"></i>
                    </span>
                    <input type="number" class="form-control" readonly name="product-quantity" value="<?php echo remove_junk($product['quantity']); ?>">
                  </div>
                </div>
              </div> -->
              <!-- <div class="col-md-4">
                  <div class="form-group">
                    <label for="qty">Precio de compra</label>
                    <div class="input-group">
                      <span class="input-group-addon">
                        <i class="glyphicon glyphicon-usd"></i>
                      </span>
                      <input type="number" class="form-control"readonly name="buying-price" value="<?php echo remove_junk($product['buy_price']);?>">
                      <span class="input-group-addon">.00</span>
                   </div>
                  </div>
                 </div>
                  <div class="col-md-4">
                   <div class="form-group">
                     <label for="qty">Precio de venta</label>
                     <div class="input-group">
                       <span class="input-group-addon">
                         <i class="glyphicon glyphicon-usd"></i>
                       </span>
                       <input type="number" class="form-control" readonly name="saleing-price" value="<?php echo remove_junk($product['sale_price']);?>">
                       <span class="input-group-addon">.00</span>
                    </div>
                   </div>
                  </div> -->
            </div>
          </div>
          <button type="submit" name="product" class="btn btn-danger">Actualizar</button>
        </form>
      </div>
      <div class="col-md-5 display-product">
        <?php if ($media['file_name'] === '0') : ?>
          <img class="img-product img-display" src="uploads/products/no_image.jpg" alt="">
        <?php else : ?>
          <img class="img-product img-display" src="uploads/products/<?php echo $media['file_name']; ?>" alt="">
        <?php endif; ?>
      </div>
      <div class="col-md-5 display-product">
        <img  class="bar-code" src="./layouts/barcode.php?text=<?php echo remove_junk($product['code']); ?>&size=50&orientation=horizontal&codetype=Code128&print=true&sizefactor=1" />
      </div>
    </div>
  </div>
</div>

<?php include_once('layouts/footer.php'); ?>