<?php
$page_title = 'Lista de productos';
require_once('includes/load.php');
// Checkin What level user has permission to view this page
page_require_level(1);

$modulo=5;
require_once('permiso.php');
/*
require_once('includes/conex.php');
$query_RolProveedor = "SELECT RolVer, RolAgregar, RolEditar, RolEliminar, UserId, ModuloId FROM rol WHERE UserId ='{$_SESSION['UsuarioId']}' AND ModuloId=5";
$RolProveedor = mysqli_query($conex, $query_RolProveedor) or die(mysqli_error($conex));
$row_RolProveedor = mysqli_fetch_assoc($RolProveedor);
*/

$clientes = find_all('clientes');
?>
<?php include_once('layouts/header.php'); 
if($row_permiso['RolVer']==0) {echo "No tiene permiso ";} else { 
?>

<div class="row">
  <div class="col-md-12">
    <?php echo display_msg($msg); ?>
  </div>
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading clearfix">
        <div class="pull-right">
          <a href="add_cliente.php" class="btn btn-primary" <?php if($row_permiso['RolAgregar']==0){echo 'style="pointer-events: none"';} ?> >Agregar Cliente</a>
        </div>
      </div>
      <div class="panel-body">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th class="text-center" style="width: 10%;">#</th>
              <th class="text-center" style="width: 50px;"> Nombre </th>
              <th class="text-center" style="width: 50px;"> Dni </th>
              <th class="text-center" style="width: 50px;"> Direccion </th>
              <th class="text-center" style="width: 50px;"> Accion </th>
              <th class="text-center" style="width: 10%;"> Estado </th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($clientes as $cli) : ?>
              <tr>
                <td class="text-center"><?php echo count_id(); ?></td>
                <td class="text-center"> <?php echo remove_junk($cli['ClienteNombre']); ?></td>
                <td class="text-center"> <?php echo remove_junk($cli['ClienteDNI']); ?></td>
                <td class="text-center"> <?php echo remove_junk($cli['ClienteDireccion']); ?></td>
                <td class="text-center">
                  <div class="btn-group">
                    <a href="edit_cliente.php?id=<?php echo (int)$cli['ClienteId']; ?>" <?php if($row_permiso['RolEditar']==0){echo 'style="pointer-events: none"';} ?> class="btn btn-info btn-xs" title="Editar" data-toggle="tooltip">
                      <span class="glyphicon glyphicon-edit"></span>
                    </a>
                  </div>
                </td>
                <td class="text-center">
                  <?php if ($cli['ClienteStatus'] === '1') : ?>
                    <span class="label label-success"><?php echo "Activo"; ?></span>
                  <?php else : ?>
                    <span class="label label-danger"><?php echo "Inactivo"; ?></span>
                  <?php endif; ?>
                </td>
              </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<?php }?>
<?php include_once('layouts/footer.php'); ?>