<?php
$page_title = 'Agregar compra';
require_once('includes/load.php');
require_once('includes/conex.php');


if (!function_exists("GetSQLValueString")) {
  function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
  {
    global $conex;
    if (PHP_VERSION < 6) {
      $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
    }
    $theValue = function_exists("mysqli_real_escape_string") ? mysqli_real_escape_string($conex, $theValue) : mysqli_escape_string($conex, $theValue);

    switch ($theType) {
      case "text":
        $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
        break;
      case "long":
      case "int":
        $theValue = ($theValue != "") ? intval($theValue) : "NULL";
        break;
      case "double":
        $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
        break;
      case "date":
        $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
        break;
      case "defined":
        $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
        break;
    }
    return $theValue;
  }
}
date_default_timezone_set('America/Lima');
$fecha=date("Y-m-d");
$hora= date('H:i:s');
$hoy= $fecha." ".$hora;

$numerocompra = 0;
$total = 0;
$mensaje = "";
$all_movimientos = find_all('moventrada');

$query_proveedor = "SELECT id, name FROM proveedores where status ='1'";
$proveedor = mysqli_query($conex, $query_proveedor) or die(mysqli_error($conex));
$row_proveedor = mysqli_fetch_assoc($proveedor);

$query_productos = "SELECT id, product_id, qty, price, ProveedorId, CompraTotal FROM shop WHERE CompraEstado = 0";
$productos = mysqli_query($conex, $query_productos) or die(mysqli_error($conex));
$row_productos = mysqli_fetch_assoc($productos);
$totalRows_productos = mysqli_num_rows($productos);

$query_descripcion = "SELECT CompraDescripcion, CompraDoc FROM shop ORDER BY id DESC LIMIT 1";
$descripcion = mysqli_query($conex, $query_descripcion) or die(mysqli_error($conex));
$row_descripcion= mysqli_fetch_assoc($descripcion); 

if (isset($_POST['agregar'])) {

  $query_ultimacompra = "SELECT id, input_part, CompraEstado FROM shop ORDER BY id DESC LIMIT 1 ";
  $ultimacompra = mysqli_query($conex, $query_ultimacompra) or die(mysqli_error($conex));
  $row_ultimacompra = mysqli_fetch_assoc($ultimacompra);
  $totalRows_ultimacompra = mysqli_num_rows($ultimacompra);
  echo $totalRows_ultimacompra;
  if ($totalRows_ultimacompra == 0) {
    $numerocompra = 1;
  } else {
    if ($row_ultimacompra['CompraEstado'] == 0) {
      $numerocompra = $row_ultimacompra['input_part'];
    } else {
      $numerocompra = $row_ultimacompra['input_part'] + 1;
    }
  }

  $query_buscaproducto = "SELECT  id, name, sale_price, quantity, buy_price  FROM products WHERE name ='{$_POST['title']}'";
  $buscaproducto = mysqli_query($conex, $query_buscaproducto) or die(mysqli_error($conex));
  $row_buscaproducto = mysqli_fetch_assoc($buscaproducto);
  $CompraTotal = $_POST['Cantidad'] * $row_buscaproducto['buy_price'];
  $stock = $row_buscaproducto['quantity'] + $_POST['Cantidad'];
  if ($stock >= 0) {
    $insertSQL = sprintf(
      "INSERT INTO shop 
      ( product_id, qty, price, ProveedorId, CompraDescripcion, MovEntradaId, CompraDoc, CompraTotal, input_part, date, CompraUsuario, ProductoStock, CompraPU) 
      VALUES (  %s, %s, %s,%s, %s, %s, %s, %s,
      '{$numerocompra}', '{$hoy}', '{$_SESSION['user_id']}', '{$stock}', '{$row_buscaproducto['buy_price']}')",
      GetSQLValueString($row_buscaproducto['id'], "int"),
      GetSQLValueString($_POST['Cantidad'], "int"),
      GetSQLValueString($row_buscaproducto['buy_price'], "int"),
      GetSQLValueString($_POST['ProveedorId'], "int"),
      GetSQLValueString($_POST['CompraDescripcion'], "text"),
      GetSQLValueString($_POST['CompraMovimiento'], "text"),
      GetSQLValueString($_POST['CompraDoc'], "text"),
      GetSQLValueString($CompraTotal, "int")
    );

    $Result1 = mysqli_query($conex, $insertSQL) or die(mysqli_error($conex));

    $stock = $row_buscaproducto['quantity'] + $_POST['Cantidad'];

    $updateSQL = "UPDATE products SET quantity='{$stock}'  WHERE id='{$row_buscaproducto['id']}'";
    mysqli_select_db($conex, $database_conex);
    $Result1 = mysqli_query($conex, $updateSQL) or die(mysqli_error($conex));

    $updateGoTo = "compra.php?ProveedorId=" . $_POST['ProveedorId'];
    header("Location:" . $updateGoTo);
  } else {
    $updateGoTo = "compra.php?m=1&ProveedorId=" . $_POST['ProveedorId'];
    header("Location:" . $updateGoTo);
  }
}

if (isset($_GET['id'])) {
  $query_buscaproducto = "SELECT  id, name, sale_price, quantity FROM products WHERE id ='{$_GET['ProductoId']}'";
  $buscaproducto = mysqli_query($conex, $query_buscaproducto) or die(mysqli_error($conex));
  $row_buscaproducto = mysqli_fetch_assoc($buscaproducto);

  $stockR = $row_buscaproducto['quantity'] - $_GET['Cantidad'];

  $updateSQL = "UPDATE products SET quantity='{$stockR}'  WHERE id='{$_GET['ProductoId']}'";
  echo $updateSQL;
  mysqli_select_db($conex, $database_conex);
  $Result1 = mysqli_query($conex, $updateSQL) or die(mysqli_error($conex));

  $deleteSQL = "DELETE FROM shop WHERE id='{$_GET['id']}'";
  $Result1 = mysqli_query($conex, $deleteSQL) or die(mysqli_error());


  if ($totalRows_productos == 1) {

    $updateGoTo = "compra.php";
  } else {
    $updateGoTo = "compra.php?ProveedorId=". $_GET['ProveedorId'];
  }

  header("Location:" . $updateGoTo);
}

if (isset($_GET['total'])) {
  $updateSQL = "UPDATE shop SET CompraEstado=1  WHERE CompraEstado=0";
  mysqli_select_db($conex, $database_conex);
  $Result1 = mysqli_query($conex, $updateSQL) or die(mysqli_error($conex));

  $query_ultimacompra = "SELECT input_part FROM shop ORDER BY id DESC LIMIT 1 ";
  $ultimacompra = mysqli_query($conex, $query_ultimacompra) or die(mysqli_error($conex));
  $row_ultimacompra = mysqli_fetch_assoc($ultimacompra);


  $updateGoTo = "compra_detalle.php?compra=" . $row_ultimacompra['input_part'];
  header("Location:" . $updateGoTo);
}

if (isset($_GET['veliminacompra'])) {
  $deleteSQL = "DELETE FROM shop WHERE CompraEstado=0";
  $Result1 = mysqli_query($conex, $deleteSQL) or die(mysqli_error($conex));

  $updateGoTo = "compra.php";
  header("Location:" . $updateGoTo);
}

$modulo=10;
require_once('permiso.php')
?>
<?php include_once('layouts/header.php'); ?>
  <?php if($row_permiso['RolVer']==0) {echo "No tiene permiso ";} else { ?>
<form action="compra.php" method="post" name="form1" id="form1" autocomplete="off">
  <div class="row">
    <div class="col-md-6">
      <div class="form-group">
        <div class="input-group">
          <span class="input-group-btn">
            <span class="btn btn-primary">Proveedor</span>
          </span>
          <select name="ProveedorId" id="ProveedorId" class="form-control" required>
            <option >Seleccione un proveedor</option>
            <?php do { ?>
              <option value="<?php echo $row_proveedor['id'] ?>" <?php
                                                                      if (isset($_GET['id'])) {
                                                                        if ($row_proveedor['id'] == $_GET['id']) {
                                                                          echo "selected";
                                                                        }
                                                                      }
                                                                      ?>>
                <?php echo $row_proveedor['name'] ?></option>
            <?php } while ($row_proveedor = mysqli_fetch_assoc($proveedor)); ?>
          </select>
        </div>
        <br>
        
        <div class="input-group">
          <span class="input-group-btn">
            <button type="submit" class="btn btn-primary">Documento</button>
          </span>
          <input name="CompraDoc" type="text" placeholder="Ingresa Documento de Compra" class="form-control" id="CompraDoc" value="<?php if (isset($_GET['ProveedorId'])) {
                                                                                                          echo $row_descripcion['CompraDoc'];
                                                                                                        } ?>" required>
        </div>
        <br>
        <div class="input-group">
          <span class="input-group-btn">
            <button type="submit" class="btn btn-primary">Búsqueda</button>
          </span>
          <input type="text" id="sug_input" class="form-control" name="title" placeholder="Buscar por el nombre del producto" required>
        </div>
       
        <div id="result" class="list-group" style="margin: auto;"></div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group ">
        <div class="input-group">
          <span class="input-group-btn">
            <button type="submit" class="btn btn-primary">Cantidad&nbsp;&nbsp;&nbsp;&nbsp;</button>
          </span>
          <input name="Cantidad" placeholder="Ingresar Cantidad de Items"  type="number" class="form-control" id="Cantidad" required>
        </div>
        <br>
        <div class="input-group">
          <span class="input-group-btn">
            <span class="btn btn-primary">Movimiento</span>
          </span>
          <select class="form-control" name="CompraMovimiento" id="CompraMovimiento" required>
                    <option value="">Selecciona un Tipo de  Movieminto</option>
                    <?php foreach ($all_movimientos as $mov) : ?>
                      <option value="<?php echo (int)$mov['id'] ?>">
                        <?php echo $mov['name'] ?></option>
                    <?php endforeach; ?> 
                  </select>
          
        </div>
        
        <br>
        <div class="input-group">
          <span class="input-group-btn">
            <button type="submit" class="btn btn-primary">Descripción</button>
          </span>
          <input name="CompraDescripcion" type="text"  placeholder="Agregar una breve descripción refrerente del ingreso" class="form-control" id="CompraDescripcion" value="<?php if (isset($_GET['ProveedorId'])) {
                                                                                                          echo $row_descripcion['CompraDescripcion'];
                                                                                                        } ?>" required>
        </div>
          
      </div>


      <?php if (isset($_GET['m'])) { ?>
        <span class="btn btn-danger">Ocurrio un Error</span>
      <?php }

      if (!isset($_GET['ProveedorId']) && $totalRows_productos > 0) { ?>
        <a href="compra.php?veliminacompra=1" class="btn btn-danger">Elimine productos antes de <br>hacer una nueva compra</a>
      <?php } ?>
    </div>
  </div>
  <div class="btn-add">
  <a href="registro_compras.php" class="pull-right btn btn-primary">Ver Compras</a>
        <input name="agregar" type="submit" class="pull-right btn btn-danger" id="agregar" value="Agregar" <?php if($row_permiso['RolAgregar']==0){echo 'style="pointer-events: none"';} ?>>
  </div>
   
</form>

<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading clearfix">
        <strong>
          <span class="glyphicon glyphicon-th"></span>
          <span>Productos</span>
        </strong>
      </div>
      <div class="panel-body">
        <form method="post" action="add_sale.php">
         
          <table class="table table-bordered">
            <thead>
              <th style="width: 50%;" colspan="3"> Producto </th>
              <!-- <th> Precio </th> -->
              <th>
                <div align="center">Cantidad </div>
              </th>
              <!-- <th>
                <div align="right">Total </div>
              </th> -->
              <th>
              <div align="center">Acción </div>
              </th>
            </thead>
            <tbody id="product_info"> </tbody>
            <?php
            if ($totalRows_productos <> 0) {
              do {
                $query_productolista = "SELECT name, buy_price FROM products WHERE id = '{$row_productos['product_id']}'";
                $productolista = mysqli_query($conex, $query_productolista) or die(mysqli_error($conex));
                $row_productolista = mysqli_fetch_assoc($productolista);
            ?>
                <tr>
                  <td colspan="3"> <?php echo $row_productolista['name']; ?> </td>
                  <!-- <td> <?php echo $row_productolista['buy_price']; ?> </td> -->
                  <td>
                    <div align="center"><?php echo $row_productos['qty']; ?> </div>
                  </td>
                  <!-- <td>
                    <div align="right">
                      <?php
                      $total = $row_productolista['buy_price'] * $row_productos['qty'] + $total;
                      echo number_format($row_productolista['buy_price'] * $row_productos['qty'], 2, '.', ','); ?>
                    </div>
                  </td> -->
                  <td align="center">
                    <?php
                    if (!isset($_GET['ProveedorId']) && $totalRows_productos > 0) {
                    } else { ?>
                      <a href="compra.php?id=<?php echo $row_productos['id']; ?>&ProveedorId=<?php echo $_GET['ProveedorId'] ?>&Cantidad=<?php echo $row_productos['qty'] ?>&ProductoId=<?php echo $row_productos['product_id'] ?>" class="btn btn-danger btn-xs" title="Eliminar" data-toggle="tooltip"> <span class="glyphicon glyphicon-trash"></span> </a>
                    <?php } ?>
                  </td>
                </tr>
              <?php } while ($row_productos = mysqli_fetch_assoc($productos)); ?>
              <tr>
                <td colspan="4"> </td>
                <!-- <td>
                  <div align="right"><span style="font-weight: bold">TOTAL</span></div>
                </td>
                <td>
                  <div align="right"><?php echo number_format($total, 2, '.', ','); ?> </div>
                </td> -->
                <td>
                <div align="center">
                  <?php if (!isset($_GET['ProveedorId']) && $totalRows_productos > 0) {
                            } else { ?>
                              <a href="compra.php?total=<?php echo $total; ?>" class="btn btn-primary">Cerrar Compra</a>
                            <?php } ?>
                  </div>
                </td>
              </tr>
            <?php } ?>
          </table>
        </form>
      </div>
    </div>
  </div>

</div>

<?php  } include_once('layouts/footer.php'); ?>

<?php
mysqli_free_result($proveedor);
mysqli_free_result($productos);
mysqli_free_result($descripcion);
@mysqli_free_result($ultimacompra);
@mysqli_free_result($buscaproducto);

?>