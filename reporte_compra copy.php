<?php
require_once('includes/conex.php');
require_once('includes/load.php');

$numeroventa=0;
$total = 0;


$row_compra= find_by_part_number('shop',$_GET['compra']);

$query_movimientos = find_by_id('moventrada',(int)$row_compra['MovEntradaId']); 
$query_productos = "SELECT id, product_id, qty, price, CompraTotal FROM shop WHERE input_part = '{$_GET['compra']}'";
$productos = mysqli_query($conex, $query_productos) or die(mysqli_error($conex));
$row_productos= mysqli_fetch_assoc($productos); 
// $row_productos= find_by_part_number('shop',$_GET['compra']);
$row_proveedores = find_by_id('proveedores',$row_compra['ProveedorId']);
$query_descripcion = "SELECT CompraDescripcion, CompraDoc FROM shop ORDER BY id DESC LIMIT 1";
$descripcion = mysqli_query($conex, $query_descripcion) or die(mysqli_error($conex));
$row_descripcion= mysqli_fetch_assoc($descripcion); 

$query_user = "SELECT name FROM users WHERE id ='{$row_compra['CompraUsuario']}'";
$user = mysqli_query($conex, $query_user) or die(mysqli_error($conex));
$row_user = mysqli_fetch_assoc($user);	

?>
<style type="text/css">
<!--
.Estilo6 {
	color: #FFFFFF;
	font-weight: bold;
}
.Estilo8 {color: #000000; }
.Estilo9 {
	color: #9eb6e4;
	font-weight: bold;
}
-->
</style>

<h2 align="center" class="Estilo9">Detalle de Compra</h2>
<hr  style="color:#9eb6e4"/>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
<td height="20"><span style="font-weight: bold">Parte de Entrada</span></td>
<td height="20"><?php printf('%08d', $row_compra['input_part']); ?></td>
</tr>
<tr>
<td height="20"><span style="font-weight: bold">Tipo de Movimiento</span></td>
<td height="20"><?php echo $query_movimientos['name']; ?></td>
</tr>
<tr>
<td width="30%" height="20"><span style="font-weight: bold">Nombre:</span></td>
<td height="20"><?php echo $row_proveedores['name']; ?></td>
</tr>
<tr>
<td height="20"><span style="font-weight: bold">Dirección:</span></td>
<td height="20"><?php echo $row_proveedores['address']; ?></td>
</tr>
<tr>
<td height="20"><span style="font-weight: bold">Fecha:</span></td>
<td height="20"><?php echo $row_compra['date']; ?></td>
</tr>
<tr>
<td height="20"><span style="font-weight: bold">Descripción:</span> </td>
<td height="20"><?php echo $row_compra['CompraDescripcion']; ?></td>
</tr>
<tr>
<td height="20"><span style="font-weight: bold">Doc Referencia:</span> </td>
<td height="20"><?php echo $row_compra['CompraDoc']; ?></td>
</tr>
<tr>
  <td height="20"><strong>Usuario:</strong></td>
  <td height="20"><?php echo $row_user['name']; ?></td>
</tr>
</table>     

<br>

<table width="100%"  cellpadding="0" cellspacing="0" >
<tr>
<td height="20" bordercolor="1" bgcolor="#9eb6e4" colspan="3"><span class="Estilo6"> Producto </span></td>
<!--<td height="20" bordercolor="1" bgcolor="#9eb6e4"><div align="right"><span class="Estilo6"> Precio </span></div></td>-->
<td height="20" bordercolor="1" bgcolor="#9eb6e4"><div align="center"><span class="Estilo6"> Cantidad </span></div></td>
<!--<td height="20" bordercolor="1" bgcolor="#9eb6e4"><div align="right"><span class="Estilo6"> Total </span></div></td>-->
</tr>
<?php 		

$row_productolista= find_by_id('products',$row_productos['product_id']); 
print_r($row_productolista);
?>
<tr>
<td height="20" bordercolor="1" colspan="3"> <?php echo $row_productolista['name']; ?> </td>
<!--<<td height="20" bordercolor="1"> <div align="right"><?php echo $row_productolista['sale_price']; ?> </div></td>-->
<td height="20" bordercolor="1"> <div align="center"><?php echo $row_productos['qty']; ?> </div></td>
<!--<<td height="20" bordercolor="1"> <div align="right">
  <?php
$total1 = $row_productolista['sale_price'] * $row_productos['qty'] + $total; 
$total =  $row_productos['qty'] + $total; 
echo number_format($row_productolista['sale_price'] * $row_productos['qty'], 2, '.',','); ?> 
</div></td>-->
</tr>           
<?php ?>
<tr>
<td height="20" bordercolor="1"> </td>
<td height="20" bordercolor="1"> <div align="right"></div></td>
<td height="20" bordercolor="1" bgcolor="#F3F3F3"> <div align="right"><span class="Estilo8"style="font-weight: bold" >Total</span></div></td>
<td height="20" bordercolor="1" bgcolor="#F3F3F3"><div align="center"><span class="Estilo8" style="font-weight: bold"><?php echo number_format($total, 0, '.',',');; ?></span> </div></td>
</tr>     
</table>

<?php
mysqli_free_result($descripcion);
mysqli_free_result($user);
?>
