<?php 

require_once 'lib/autoload.inc.php';

use Dompdf\Dompdf;

$dompdf = new Dompdf();
ob_start();
if(isset($_GET['compra'])){
    include "./reporte_compra.php";
}
if(isset($_GET['venta'])){
    include "./reporte_venta.php";
}
if(isset($_GET['reporte_venta'])){
    include "./sale_report_process.php";
}
if(isset($_GET['prod'])){
    include "./reporte_kardex.php";
}
$html = ob_get_clean();
$dompdf->loadHtml($html);
if(isset($_GET['prod']))
{	
	$dompdf->setPaper('A4', 'landscape');
}
else
{
		$dompdf->setPaper('A4', 'portrait');
}
$dompdf->render();
header("Content-type: application/pdf");
header("Content-Disposition: inline; filename=reporte.pdf");
echo $dompdf->output();
