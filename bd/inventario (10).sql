-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 05-12-2021 a las 23:32:32
-- Versión del servidor: 10.4.19-MariaDB
-- Versión de PHP: 7.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `inventario`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categories`
--

CREATE TABLE `categories` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(60) NOT NULL,
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `categories`
--

INSERT INTO `categories` (`id`, `name`, `status`) VALUES
(1, 'Routers', 1),
(2, 'Access points', 0),
(3, 'Switchs', 1),
(4, 'Cámaras', 1),
(7, 'Perifericos', 1),
(8, 'Laptops', 1),
(10, 'Routerss', 1),
(14, 'Conectores Red Cat6', 1),
(15, 'Conectores Red Cat5', 1),
(16, 'Pernos para tensión Torre', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `ClienteId` int(11) NOT NULL,
  `ClienteNombre` varchar(80) NOT NULL,
  `ClienteDNI` varchar(8) NOT NULL,
  `ClienteDireccion` varchar(100) NOT NULL,
  `ClienteStatus` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`ClienteId`, `ClienteNombre`, `ClienteDNI`, `ClienteDireccion`, `ClienteStatus`) VALUES
(1, 'Juan Jose', '8463255', 'Av La Molina 123', 0),
(2, 'Maria Castro', '0598746', 'Jr Pasco 2020', 1),
(3, 'Esteban Murillo', '21549365', 'Av San Juan 234', 1),
(4, 'Alex Bazan', '02558966', 'Calle Uno 556', 1),
(5, 'Cliente2', '58965875', 'Miraflores', 1),
(6, 'Cliente3', '45985662', 'Lima', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `manufacturer`
--

CREATE TABLE `manufacturer` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `status` int(1) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `manufacturer`
--

INSERT INTO `manufacturer` (`id`, `name`, `status`) VALUES
(1, 'TP-LINK', 1),
(2, 'UBIQUITI', 1),
(3, 'DAHUA', 1),
(4, 'D-LINK', 0),
(5, 'CISCO', 1),
(6, 'HUAWEI', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `media`
--

CREATE TABLE `media` (
  `id` int(11) UNSIGNED NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `file_type` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `media`
--

INSERT INTO `media` (`id`, `file_name`, `file_type`) VALUES
(2, 'AX11000.png', 'image/jpeg'),
(4, 'mesh.png', 'image/png'),
(5, 'tapo_c310.png', 'image/png'),
(6, 'T2600G-28TS.png', 'image/png'),
(7, 'Huawei.png', 'image/png'),
(8, 'conectores.jpg', 'image/jpeg'),
(9, 'perno tension.png', 'image/png'),
(10, 'conec cat6.jpg', 'image/jpeg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `moventrada`
--

CREATE TABLE `moventrada` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(60) NOT NULL,
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `moventrada`
--

INSERT INTO `moventrada` (`id`, `name`, `status`) VALUES
(1, 'Compra', 1),
(2, 'Transferencia', 1),
(3, 'Ajuste', 1),
(4, 'Garantia', 1),
(7, 'Reingreso', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `movsalida`
--

CREATE TABLE `movsalida` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(60) NOT NULL,
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `movsalida`
--

INSERT INTO `movsalida` (`id`, `name`, `status`) VALUES
(1, 'Venta', 1),
(2, 'Transferencia', 1),
(3, 'Ajuste Salida', 1),
(4, 'Garantia', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permisos`
--

CREATE TABLE `permisos` (
  `id` int(11) NOT NULL,
  `modulo` varchar(150) NOT NULL,
  `rol_id` int(11) NOT NULL,
  `c` int(2) NOT NULL,
  `r` int(2) NOT NULL,
  `u` int(2) NOT NULL,
  `d` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `permisos`
--

INSERT INTO `permisos` (`id`, `modulo`, `rol_id`, `c`, `r`, `u`, `d`) VALUES
(1, 'ventas', 1, 0, 1, 0, 0),
(2, 'productos', 1, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `products`
--

CREATE TABLE `products` (
  `id` int(11) UNSIGNED NOT NULL,
  `code` varchar(11) NOT NULL,
  `manu_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `quantity` varchar(50) DEFAULT NULL,
  `buy_price` decimal(25,2) DEFAULT NULL,
  `sale_price` decimal(25,2) NOT NULL,
  `categorie_id` int(11) UNSIGNED NOT NULL,
  `pmedida` varchar(20) NOT NULL,
  `media_id` int(11) DEFAULT 0,
  `date` datetime NOT NULL,
  `file_type` varchar(100) NOT NULL,
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `products`
--

INSERT INTO `products` (`id`, `code`, `manu_id`, `name`, `quantity`, `buy_price`, `sale_price`, `categorie_id`, `pmedida`, `media_id`, `date`, `file_type`, `status`) VALUES
(3, '059865X6056', 1, 'Router WRT-900 ASUS', '143', '500.00', '590.00', 1, '1', 2, '2021-10-09 07:15:46', '', 1),
(5, '059865X6057', 2, 'Sistema Wi-Fi Mesh para toda la Casa AC2200', '99', '500.00', '590.00', 1, '1', 4, '2021-10-09 07:15:46', '', 1),
(12, '059865X6058', 1, 'Cámara Inalámbrica Full HD', '0', '10.00', '10.00', 1, '2', 5, '2021-10-19 02:17:37', '', 0),
(13, '059865X6059', 2, 'Swicth 24 pts 10/100/1000', '0', '10.00', '10.00', 2, '1', 6, '2021-10-19 02:32:09', '', 1),
(14, '059865X6060', 1, 'Router WRT-500 ASUS	', '93', '10.00', '10.00', 1, '1', 2, '2021-10-19 02:39:11', '', 1),
(24, '059865X6061', 2, 'Router Gaming AX11000 Next-Gen Tri-Banda', '142', '343.00', '343.00', 3, '1', 4, '2021-10-19 20:56:10', '', 1),
(26, '059865X6063', 1, 'Router WRT-1200 ASUS	', '7', '100.00', '150.00', 1, '1', 2, '2021-10-20 08:35:59', '', 1),
(27, '059865X6064', 1, 'T1600G-16TS  Switch Administrable L2', '100', NULL, '0.00', 3, '1', 6, '2021-11-21 02:42:17', '', 0),
(28, '059865X6065', 6, 'Router Huawei Dual band AC1200', NULL, NULL, '0.00', 1, '2', 7, '2021-11-21 03:20:37', '', 0),
(29, '059865X6066', 2, 'Pernos de tensión para Torre por Kilo', NULL, NULL, '0.00', 16, '5', 9, '2021-11-24 01:49:44', '', 1),
(30, '059865X6067', 3, 'Conectores UTP CAT 6 ', NULL, NULL, '0.00', 14, '2', 10, '2021-11-28 11:51:48', '', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedores`
--

CREATE TABLE `proveedores` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` int(10) NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `address` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `proveedores`
--

INSERT INTO `proveedores` (`id`, `name`, `phone`, `email`, `status`, `address`) VALUES
(1, 'Alicorp', 912946904, 'ventas@Alicorp.com', 0, 'Calle España 123'),
(7, 'SODIMAC', 555555, 'ventas@sodimac.com', 1, 'LIMA 10'),
(10, 'MAESTRO', 12345678, 'ventas@maestro.com', 1, 'PISCO'),
(13, 'prueba', 800, 'servicios@banexcoin.com', 1, 'Lima'),
(14, 'Pr3', 2147483647, 'dsdsds@sdsd.com', 1, 'lima');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `nombre_rol` varchar(200) NOT NULL,
  `status` int(2) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `nombre_rol`, `status`) VALUES
(1, 'ventas', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sales`
--

CREATE TABLE `sales` (
  `id` int(11) UNSIGNED NOT NULL,
  `input_part` int(11) UNSIGNED ZEROFILL NOT NULL,
  `MovSalidaId` int(11) UNSIGNED NOT NULL,
  `product_id` int(11) UNSIGNED NOT NULL,
  `qty` int(11) NOT NULL,
  `price` decimal(25,2) NOT NULL,
  `date` datetime NOT NULL,
  `ClienteId` int(11) NOT NULL,
  `VentaTotal` decimal(12,2) NOT NULL,
  `VentaEstado` int(11) NOT NULL,
  `VentaDoc` varchar(100) NOT NULL,
  `VentaDescripcion` varchar(150) NOT NULL,
  `VentaUsuario` varchar(20) NOT NULL,
  `Tipo` varchar(1) NOT NULL DEFAULT 'V',
  `VentaPU` decimal(12,2) NOT NULL,
  `ProductoStock` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sales`
--

INSERT INTO `sales` (`id`, `input_part`, `MovSalidaId`, `product_id`, `qty`, `price`, `date`, `ClienteId`, `VentaTotal`, `VentaEstado`, `VentaDoc`, `VentaDescripcion`, `VentaUsuario`, `Tipo`, `VentaPU`, `ProductoStock`) VALUES
(88, 00000000001, 3, 12, 10, '10.00', '2001-11-16 19:22:22', 2, '100.00', 1, 'GS001-000369', 'AJUSTE DE SALIDA', '30', 'V', '10.00', 0),
(89, 00000000002, 1, 24, 1, '343.00', '2021-11-16 19:49:38', 2, '343.00', 1, 'GS001-000503', 'VENTA 1', '30', 'V', '343.00', 99),
(90, 00000000002, 1, 5, 1, '590.00', '2021-11-16 19:49:50', 2, '590.00', 1, 'GS001-000503', 'VENTA 1', '30', 'V', '590.00', 99),
(93, 00000000003, 1, 24, 7, '343.00', '2021-11-21 20:55:23', 3, '2401.00', 1, 'GS001-000504', 'Prueba 6', '30', 'V', '343.00', 92),
(95, 00000000004, 2, 3, 7, '590.00', '2021-11-21 22:54:27', 4, '4130.00', 1, 'GS 006-000265', 'TRANSFERENCIA AL ALMACEN 2', '30', 'V', '590.00', 93);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `shop`
--

CREATE TABLE `shop` (
  `id` int(11) UNSIGNED NOT NULL,
  `input_part` int(11) UNSIGNED ZEROFILL NOT NULL,
  `MovEntradaId` int(11) UNSIGNED NOT NULL,
  `product_id` int(11) UNSIGNED NOT NULL,
  `qty` int(11) NOT NULL,
  `price` decimal(25,2) NOT NULL,
  `date` datetime NOT NULL,
  `ProveedorId` int(11) NOT NULL,
  `CompraTotal` decimal(12,2) NOT NULL,
  `CompraEstado` int(11) NOT NULL,
  `CompraDoc` varchar(100) NOT NULL,
  `CompraDescripcion` varchar(150) NOT NULL,
  `CompraUsuario` varchar(20) NOT NULL,
  `Tipo` varchar(1) NOT NULL DEFAULT 'C',
  `CompraPU` decimal(12,2) NOT NULL,
  `ProductoStock` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `shop`
--

INSERT INTO `shop` (`id`, `input_part`, `MovEntradaId`, `product_id`, `qty`, `price`, `date`, `ProveedorId`, `CompraTotal`, `CompraEstado`, `CompraDoc`, `CompraDescripcion`, `CompraUsuario`, `Tipo`, `CompraPU`, `ProductoStock`) VALUES
(51, 00000000001, 3, 12, 10, '10.00', '2001-11-16 19:21:56', 7, '100.00', 1, 'FT 001-000369', 'AJUSTE PARA PRUEBA DE SUSPENCION DE PRODUCTO', '30', 'C', '10.00', 10),
(52, 00000000002, 1, 3, 100, '500.00', '2021-11-16 19:28:57', 10, '50000.00', 1, 'FT002-003689', 'COMPRA INICIAL', '30', 'C', '500.00', 100),
(53, 00000000002, 1, 5, 100, '500.00', '2021-11-16 19:29:05', 7, '50000.00', 1, 'FT002-003689', 'COMPRA INICIAL', '30', 'C', '500.00', 100),
(54, 00000000002, 1, 14, 100, '10.00', '2021-11-16 19:48:32', 7, '1000.00', 1, 'FT002-003689', 'COMPRA INICIAL', '30', 'C', '10.00', 100),
(55, 00000000002, 1, 24, 100, '343.00', '2021-11-16 19:48:40', 7, '34300.00', 1, 'FT002-003689', 'COMPRA INICIAL', '30', 'C', '343.00', 100),
(58, 00000000003, 7, 26, 7, '100.00', '2021-11-21 22:42:21', 10, '700.00', 1, 'GS004-000698', 'Devolución de productos', '30', 'C', '100.00', 7),
(59, 00000000004, 1, 3, 50, '500.00', '2021-11-30 19:09:39', 7, '25000.00', 1, 'FT005-003684', 'Compra para Stock', '30', 'C', '500.00', 143),
(60, 00000000004, 1, 24, 50, '343.00', '2021-11-30 19:09:55', 7, '17150.00', 1, 'FT005-003684', 'Compra para Stock', '30', 'C', '343.00', 142);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `unidadmedida`
--

CREATE TABLE `unidadmedida` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(60) NOT NULL,
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `unidadmedida`
--

INSERT INTO `unidadmedida` (`id`, `name`, `status`) VALUES
(1, 'Unidad', 1),
(2, 'Caja', 1),
(3, 'Metro', 1),
(4, 'Centímetro', 1),
(5, 'Kilogramo (KG)', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(60) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `user_level` int(11) NOT NULL,
  `image` varchar(255) DEFAULT 'no_image.jpg',
  `status` int(1) NOT NULL,
  `date` datetime NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `create_by` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `password`, `user_level`, `image`, `status`, `date`, `last_login`, `create_by`) VALUES
(2, 'Erick', 'Erick', '8cb2237d0679ca88db6464eac60da96345513964', 1, 'b1qb5izb10.png', 1, '2021-10-01 00:35:49', '2021-10-20 06:14:19', 'Admin'),
(30, 'AdminCS', 'Admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', 1, 'cl7g0tji30.png', 1, '2021-10-01 00:35:49', '2021-12-05 23:31:46', 'Erick'),
(36, 'Test', 'Admin2', '8cb2237d0679ca88db6464eac60da96345513964', 2, 'ovu9u1fp36.png', 1, '2021-10-01 00:35:49', '2021-11-02 23:23:38', 'Admin'),
(45, 'Admin6', 'Admin6', '8cb2237d0679ca88db6464eac60da96345513964', 3, 'xkrey9ua45.png', 1, '2021-10-20 10:25:45', '2021-11-02 23:23:55', 'Admin');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_groups`
--

CREATE TABLE `user_groups` (
  `id` int(11) NOT NULL,
  `group_name` varchar(150) NOT NULL,
  `group_level` int(11) NOT NULL,
  `group_status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `user_groups`
--

INSERT INTO `user_groups` (`id`, `group_name`, `group_level`, `group_status`) VALUES
(1, 'Administrador', 1, 1),
(3, 'Ventas', 3, 1),
(4, 'Almacen', 2, 1),
(7, 'Logistica', 4, 1),
(8, 'Gerencia', 5, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`ClienteId`);

--
-- Indices de la tabla `manufacturer`
--
ALTER TABLE `manufacturer`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`name`) USING BTREE;

--
-- Indices de la tabla `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indices de la tabla `permisos`
--
ALTER TABLE `permisos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_rol_id` (`rol_id`);

--
-- Indices de la tabla `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD UNIQUE KEY `code` (`code`),
  ADD KEY `categorie_id` (`categorie_id`),
  ADD KEY `media_id` (`media_id`),
  ADD KEY `fk_manufacturer` (`manu_id`);

--
-- Indices de la tabla `proveedores`
--
ALTER TABLE `proveedores`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `product_id` (`product_id`);

--
-- Indices de la tabla `shop`
--
ALTER TABLE `shop`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `product_id` (`product_id`);

--
-- Indices de la tabla `unidadmedida`
--
ALTER TABLE `unidadmedida`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD KEY `id` (`id`);

--
-- Indices de la tabla `user_groups`
--
ALTER TABLE `user_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `group_level` (`group_level`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `ClienteId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `manufacturer`
--
ALTER TABLE `manufacturer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `media`
--
ALTER TABLE `media`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `permisos`
--
ALTER TABLE `permisos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT de la tabla `proveedores`
--
ALTER TABLE `proveedores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `sales`
--
ALTER TABLE `sales`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=96;

--
-- AUTO_INCREMENT de la tabla `shop`
--
ALTER TABLE `shop`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT de la tabla `unidadmedida`
--
ALTER TABLE `unidadmedida`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT de la tabla `user_groups`
--
ALTER TABLE `user_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `permisos`
--
ALTER TABLE `permisos`
  ADD CONSTRAINT `fk_rol_id` FOREIGN KEY (`rol_id`) REFERENCES `roles` (`id`);

--
-- Filtros para la tabla `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `FK_products` FOREIGN KEY (`categorie_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_manufacturer` FOREIGN KEY (`manu_id`) REFERENCES `manufacturer` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
