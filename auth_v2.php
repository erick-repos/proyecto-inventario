<?php include_once('includes/load.php'); ?>
<?php
$req_fields = array('username', 'password');
validate_fields($req_fields);
$username = remove_junk($_POST['username']);
$password = remove_junk($_POST['password']);

if (empty($errors)) {
  $user = authenticate_v2($username, $password);
  echo "<script>console.log('Debug Objects: " . $user . "' );</script>";
  if ($user) :
    if ($user['status'] === '0') :
      $session->msg("d", "Disculpe Usuario no esta activo");
      redirect('index.php', false);
    endif;
    //create session with id
    $session->login($user['id']);
    //Update Sign in time
    updateLastLogIn($user['id']);
    // redirect user to group home page by user level
    if ($user['user_level'] === '1') :
      $session->msg("s", "Hola " . $user['username'] . ", Bienvenido al Sistema Logistico de Corsurp SAC.");
      redirect('admin.php', false);
    elseif ($user['user_level'] === '2') :
      $session->msg("s", "Hola " . $user['username'] . ", Bienvenido al Sistema Logistico de Corsurp SAC.");
      redirect('admin.php', false);
    elseif ($user['user_level'] === '3') :
      $session->msg("s", "Hola " . $user['username'] . ", Bienvenido al Sistema Logistico de Corsurp SAC.");
      redirect('admin.php', false);
    else :
      $session->msg("s", "Hola " . $user['username'] . ", Bienvenido al Sistema Logistico de Corsurp SAC.");
      redirect('admin.php', false);
    endif;
  else :
    $session->msg("d", "disculpe Usuario/Password incorrecto.");
    redirect('index.php', false);
  endif;
} else {

  $session->msg("d", $errors);
  redirect('index.php', false);
}

?>
