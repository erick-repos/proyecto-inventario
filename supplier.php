<?php



$page_title = 'Lista de productos';
require_once('includes/load.php');
// Checkin What level user has permission to view this page


$modulo=4;
require_once('permiso.php');

page_require_level(1);

$supplier = find_all('proveedores')





?>
<?php include_once('layouts/header.php'); 
if($row_permiso['RolVer']==0) {echo "No tiene permiso ";} else { 

?>
<div class="row">
  <div class="col-md-12">
    <?php echo display_msg($msg); ?>
  </div>
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading clearfix">
        <div class="pull-right">
          <a href="add_supplier.php" class="btn btn-primary" <?php if($row_permiso['RolAgregar']==0){echo 'style="pointer-events: none"';} ?>>Agregar proveedor</a>
        </div>
      </div>
      <div class="panel-body">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th class="text-center" style="width: 10%;">#</th>
              <th class="text-center" style="width: 50px;"> Nombre </th>
              <th class="text-center" style="width: 50px;"> Telefono </th>
              <th class="text-center" style="width: 50px;"> Email </th>
              <th class="text-center" style="width: 50px;"> Direccion </th>
              <th class="text-center" style="width: 50px;"> Accion </th>
              <th class="text-center" style="width: 10%;"> Estado </th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($supplier as $su) : ?>
              <tr>
                <td class="text-center"><?php echo count_id(); ?></td>

                <td> <?php echo remove_junk($su['name']); ?></td>
                <td class="text-center"> <?php echo remove_junk($su['phone']); ?></td>
                <td class="text-center"> <?php echo remove_junk($su['email']); ?></td>
                <td class="text-center"> <?php echo remove_junk($su['address']); ?></td>
                <td class="text-center">
                  <div class="btn-group">
                    <a href="edit_supplier.php?id=<?php echo (int)$su['id']; ?>" <?php if($row_permiso['RolEditar']==0){echo 'style="pointer-events: none"';} ?> class="btn btn-info btn-xs" title="Editar" data-toggle="tooltip">
                      <span class="glyphicon glyphicon-edit"></span>
                    </a>
                  </div>
                </td>
                <td class="text-center">
                  <?php if ($su['status'] === '1') : ?>
                    <span class="label label-success"><?php echo "Activo"; ?></span>
                  <?php else : ?>
                    <span class="label label-danger"><?php echo "Inactivo"; ?></span>
                  <?php endif; ?>
                </td>
              </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<?php }?>
<?php include_once('layouts/footer.php'); ?>