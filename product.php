<?php
  $page_title = 'Lista de productos';
  require_once('includes/load.php');
  // Checkin What level user has permission to view this page
  
    $modulo=7;
require_once('permiso.php');
   page_require_level(2);
  $products = join_product_table();
?>
<?php include_once('layouts/header.php');
if($row_permiso['RolVer']==0) {echo "No tiene permiso ";} else { 
 ?>
  <div class="row">
     <div class="col-md-12">
       <?php echo display_msg($msg); ?>
     </div>
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading clearfix">
         <div class="pull-right">
           <a href="add_product.php" class="btn btn-primary" <?php if($row_permiso['RolAgregar']==0){echo 'style="pointer-events: none"';} ?>>Agregar producto</a>
         </div>
        </div>
        <div class="panel-body">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th class="text-center" style="width: 50px;">#</th>
                <th class="text-center" style="width: 8%;"> Código</th>
                <th class="text-center" style="width: 8%;"> Imagen</th>
                <th class="text-center" style="width: 15%;"> Ud. Medida</th>
                <th class="text-center" style="width: 50%;"> Descripción </th>
                <th class="text-center" style="width: 8%;"> Fabricante </th>
                <th class="text-center" style="width: 8%;"> Categoría </th>
                <th class="text-center" style="width: 8%;"> Stock </th>
                <!-- <th class="text-center" style="width: 10%;"> Precio de compra </th>
                <th class="text-center" style="width: 10%;"> Precio de venta </th> -->
                <th class="text-center" style="width: 10%;"> Agregado </th>
                <th class="text-center" style="width: 8%;"> Status </th>
                <th class="text-center" style="width: 100px;"> Acciones </th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($products as $product):?>
              <tr>
                <td class="text-center"><?php echo count_id();?></td>
                
                <td class="text-center"> <?php echo remove_junk($product['code']); ?></td>
                <td class="text-center">
                  <?php if($product['media_id'] === '0'): ?>
                    <img class="img-avatar img-circle" src="uploads/products/no_image.jpg" alt="">
                  <?php else: ?>
                  <img class="img-avatar img-circle" src="uploads/products/<?php echo $product['image']; ?>" alt="">
                <?php endif; ?>
                </td>
                <td class="text-center"> <?php echo remove_junk($product['unidadmedida']); ?></td>
                <td> <?php echo remove_junk($product['name']); ?></td>
                <td class="text-center"> <?php echo remove_junk($product['manufacturer']); ?></td>
                <td class="text-center"> <?php echo remove_junk($product['categorie']); ?></td>
                <td class="text-center"> <?php echo remove_junk($product['quantity']); ?></td>
                <!-- <td class="text-center"> <?php echo remove_junk($product['buy_price']); ?></td>
                <td class="text-center"> <?php echo remove_junk($product['sale_price']); ?></td> -->
                <td class="text-center"> <?php echo read_date($product['date']); ?></td>
                <td class="text-center">
                    <?php if($product['status'] === '1'): ?>
                      <span class="label label-success"><?php echo "Activo"; ?></span>
                    <?php else: ?>
                      <span class="label label-danger"><?php echo "Inactivo"; ?></span>
                    <?php endif;?>
                    </td>

                <td class="text-center">
                  <div class="btn-group">
                    <a href="edit_product.php?id=<?php echo (int)$product['id'];?>"  <?php if($row_permiso['RolEditar']==0){echo 'style="pointer-events: none"';} ?> class="btn btn-info btn-xs"  title="Editar" data-toggle="tooltip"> <!--edit_product.php?id=<//?php echo (int)$product['id'];?>-->
                      <span class="glyphicon glyphicon-edit"></span>
                    </a>
                  </div>
                </td>
              </tr>
             <?php endforeach; ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <?php } include_once('layouts/footer.php'); ?>
