<?php
  $page_title = 'Salida diaria';
  require_once('includes/load.php');
  // Checkin What level user has permission to view this page
  $modulo=16;
require_once('permiso.php');
   page_require_level(3);
?>

<?php  
 $day= date('Y-m-d'.' '. '00:00:00');
 $day1= date('Y-m-d'.' '. '23:59:59');
 $sales = dailySales('sales',$day,$day1);

?>
<?php include_once('layouts/header.php'); ?>
  <?php if($row_permiso['RolVer']==0) {echo "No tiene permiso ";} else { ?>
<div class="row">
  <div class="col-md-6">
    <?php echo display_msg($msg); ?>
  </div>
</div>
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading clearfix">
          <strong>
            <span class="glyphicon glyphicon-th"></span>
            <span>Salida diaria</span>
          </strong>
        </div>
        <div class="panel-body">
          <table class="table table-bordered table-striped">
            <thead>
              <tr>
                <th class="text-center" style="width: 50px;">#</th>
                <th> Descripción </th>
                <!-- <th class="text-center" style="width: 15%;"> Cantidad vendida</th> -->
                <th class="text-center" style="width: 15%;"> Total </th>
                <th class="text-center" style="width: 15%;"> Fecha </th>
             </tr>
            </thead>
           <tbody>
             <?php foreach ($sales as $sale): ?>
             <tr>
               <td class="text-center"><?php echo count_id();?></td>
               <td><?php echo remove_junk($sale['name']); ?></td>
               <!-- <td class="text-center"><?php echo (int)$sale['qty']; ?></td> -->
               <td class="text-center"><?php echo remove_junk($sale['total_saleing_price']); ?></td>
               <td class="text-center"><?php echo date("d/m/Y H:i:s", strtotime ($sale['date'])); ?></td>
             </tr>
             <?php endforeach;?>
           </tbody>
         </table>
        </div>
        <ul class="nav nav-tabs nav-justified">
  <li class="active"><a href="daily_sales.php">Ventas</a></li>
  <li><a href="daily_compras.php">Compras</a></li>
</ul>
      </div>
    </div>
  </div>

<?php  } include_once('layouts/footer.php'); ?>
